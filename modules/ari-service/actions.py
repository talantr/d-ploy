# -*- coding: utf-8 -*-
import os
import sys
import copy
import tenjin
import shutil
from tenjin.helpers import * # move to common import
from fabric.api import env, task, run, sudo, put, local, lcd, cd

# Set cache for template processor
tenjin.Engine.cache = tenjin.MemoryCacheStorage()

@task
def configure(service=None):
    """
    Prepare configuration files for ARI service
    """
    print service.index,  service.name
    # Extract configuration files from last package
    with lcd(ENV.workspace):
        local('rpm2cpio $(ls -t1 {0} | head -n1) | cpio -idmu --quiet *.properties'.format('tallygram-backend-*.rpm'))

    context={'dbs' : []}

    # Setup ARI context
    context.update(get_ari_context(service))

    # Choose database shard for friend node service   # Need an index of current node
    context.update(get_db_context())

    # Choose database shard for friend node service   # Need an index of current node
    context.update(get_match_context())

    # Process common templates and push it on target server
    for item in service.configs:
        process_template(item, context)

    # Process templates of configs for match nodes and push it on target server
    for ctx in get_match_node_context():
        process_template('match_node.properties', ctx, ctx['mnode'].index)

def process_template(template, context, idx=''):
    """
    Process a template of configuration file for Tallygram's backend
    """
    tmpdir = os.path.join(ENV.workspace, env.host)

    # Add index to a file name
    tmplname = template
    if idx != '':
        tmplname, tmplext = os.path.splitext(template)
        tmplname = tmplname + str(idx) + tmplext

    # Create a temporary directory to store configuration files
    if not os.path.exists(tmpdir):
        os.mkdir(tmpdir)

    # Process template
    cfgname = os.path.join(tmpdir, tmplname)
    engine = tenjin.Engine(path=ENV.template_locations())
    with open(cfgname, 'w') as f:
        print >> f, engine.render(template, context)

def get_ari_context(service):
    """
    Get ARI endpoint.
    """
    context = {'ari' : None, 'product' : PRODUCT}
    # Setup ARI properties, only one instance of cache should be
    context['ari'] = service.localize()
    context['ari'].islocal  = 'true'
    return context

def get_db_context():
    """
    Get list of all endpoints for db shards.
    """
    context = {'dbs' : [], 'product' : PRODUCT}
    unit = PRODUCT['mysql']['db-server']
    for srv in unit.endpoints:
        if srv.hostname == env.host:
            srv = srv.localize()
        context['dbs'].append(srv)
    return context

def get_match_context():
    """
    Get a match node that will be used with ARI.
    """
    context = {'match' : None, 'mnode' :  PRODUCT['match-node'].mnode, 'product' : PRODUCT}
    unit =  PRODUCT['backend']['match']
    for srv in unit.endpoints:
        if srv.hostname == env.host:
            context['match'] = srv.localize()
            context['match'].islocal = 'true'
        else:
            context['match'] = srv
            context['match'].islocal = 'false'
        break # Only the first match service is used with ARI
    return context

def get_match_node_context():
    # Choose settings for match nodes
    context = {'mnode' : None, 'product' : env.product}
    unit = env.product['match-node'].mnode
    for srv in unit.endpoints:
        if srv.hostname == env.host:
            context['mnode'] = srv.localize()
            context['mnode'].islocal = 'true'
        else:
            context['mnode'] = srv
            context['mnode'].islocal = 'false'
        yield context
