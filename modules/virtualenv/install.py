# -*- coding: utf-8 -*-
from runner import job, Target
from fabric.api import sudo, run, puts

@job(Target.HOST)
def install(host):
    """
    Installs the python virtualenv on target server.
    """
    puts("Installing python virtualenv on {0}...".format(host))
    sudo("pip install virtualenv", shell=False)
    run("echo Virtualenv: $(virtualenv --version)", shell=False)
