# -*- coding: utf-8 -*-
import os
import re
from time import time
from fabric.api import env, task, sudo, settings, hide, local, cd, run, lcd

PKG = 'tallygram-db-*.tar.gz'
SCRIPT_NAME_RE = r'.*/([0-9]+)-[^/]+.sql$'

@task
def start(service=None):
    """
    Starts the MySQL server.
    """
    if service.daemon is not None:
        sudo("service {0} start".format(service.daemon), shell=False)

@task
def stop(service=None):
    """
    Stops the MySQL server.
    """
    # Check that service was installed
    if service.daemon is not None:
        with settings(hide("running"), warn_only=True):
            exit_code = sudo("test -f /etc/init.d/{0}".format(service.daemon), shell=False).return_code
        if exit_code == 0:
            sudo("service {0} stop".format(service.daemon), shell=False)

@task
def check(service=None):
    """
    Check state of the MySQL server.
    """
    if service.daemon is not None:
        sudo("service {0} status".format(service.daemon), shell=False)

@task
def backup():
    backup_path = "/var/onepm/db-backup"
    print 'BACKUPING DATABASES...'
    sudo("mkdir -p {0}".format(backup_path))
    unit = env.product['mysql']['db-server']
    for ep in unit.endpoints:
        if ep.hostname == env.host:
            dumpname = "dump-{0}-{1}.sql.gz".format(ep.path[1:], time())
            sudo("mysqldump --add-drop-database --create-options --databases -u root {0} | gzip > {1}{2}"\
            .format(ep.path[1:], backup_path, dumpname))

@task
def migrate():

    # Find last created package with migrations scripts
    with hide('running'):
        package = local('ls -t1 {0} | head -n1'.format(env.workspace + '/' + PKG), capture=True)

    unit = env.product['mysql']['db-server']
    print 'UPLOAD MIGRATTIONS...'
    # Copy a tar.gz package to target host and unpack it
    action.upload(package)
    with cd(action.remote_tmp):
        run('find . -mindepth 1 -maxdepth 1 -type d -exec rm -rf {} \;')
        run('ls *.tar.gz | grep -v {0} | xargs rm -rf'.format(os.path.basename(package)), shell=False)
        run('tar -zmxf $(ls -t1 {0} | head -n1)'.format(PKG))
        run('mv db/* . && rm -rf db')
        for ep in unit.endpoints:
            if ep.hostname == env.host:
                run('./migrate.sh -d {0}'.format(ep.path[1:]))
                print "Current version: ", get_last_version(package)

@task
def reset():

    # Find last created package with migrations scripts and SQL scripts
    with hide('running'):
        package = local('ls -t1 {0} | head -n1'.format(ENV.workspace + '/' + PKG), capture=True)

    unit = env.product['mysql']['db-server']
    print 'UPLOAD DUMPS FOR WITH INITIAL SCHEMA...'
    # Copy a tar.gz package to target host and unpack it
    ACTIONS.upload(package)
    with cd(ACTIONS.remote_tmp):
        # Remove all folders in working directory
        run('find . -mindepth 1 -maxdepth 1 -type d -exec rm -rf {} \;')
        run('ls *.tar.gz | grep -v {0} | xargs rm -rf'.format(os.path.basename(package)), shell=False)
        run('tar -zmxf $(ls -t1 {0} | head -n1)'.format(PKG))
        run('mv db/* . && rm -rf db')

        for ep in unit.endpoints:
            if ep.hostname == env.host:
                with hide('running'):
                    run('mysql -u root -e "DROP DATABASE IF EXISTS {0};" 2>/dev/null'.format(ep.path[1:]))
                    run('mysql -u root -e "CREATE DATABASE {0} CHARACTER SET utf8 COLLATE utf8_general_ci;" 2>/dev/null'.format(ep.path[1:]))
                    print "[{0}] Load initial schema (shard: {1}, database: {2})...".format(env.host, ep.index, ep.path[1:])
                    run("mysql -u root {0} < schema/test-data-shard-{1}.sql".format(ep.path[1:], ep.index))
                print "[{0}] Current schema version: {1}".format(env.host, get_last_version(package))

@task
def version(service=None):
    with hide('stdout', 'running'):
        print "{0}{1} {2}".format(service.uri.hostname, service.uri.path, get_version(service.uri))

def get_version(ep):
    """
    Gets a schema version from remote database
    """
    pswopt = '' if ep.password is None else '--password=' + ep.password
    schema = run("mysql -B --skip-column-names -u {0} {1} {2} -e \"SELECT value FROM meta WHERE id='migration'\" 2>/dev/null"\
    .format(ep.username, pswopt, ep.path[1:]))
    return int(schema)

def get_last_version(package=None):
    """
    Gets last schema version from a package.
    """
    with lcd(env.workspace):
        with hide('running'):
            if package is None:
                package = local('ls -t1 {0} | head -n1'.format(env.workspace + '/' + PKG), capture=True)
            script = local('tar -tf {0}  --wildcards db/migrations/*-commit*.sql | sort -r | head -1'.format(package), capture=True)
    m = re.match(SCRIPT_NAME_RE, script)
    vers = -1 if m is None else m.group(1)
    return int(vers)

def apply_migration(ep, package):
    last = get_last_version(package)
    get_version(ep)

