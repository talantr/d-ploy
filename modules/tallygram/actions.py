# -*- coding: utf-8 -*-
import os
import shutil
import tenjin
from tenjin.helpers import *
from fabric.api import env, task, run, sudo, put, local, settings, hide, lcd

@task
def start(service=None):
    """
    Starts the Tallygram service.
    """
    if service.daemon is not None:
        sudo("service {0} start".format(service.daemon), shell=False)

@task
def stop(service=None):
    """
    Stops the Tallygram service.
    """
    # Check that service was installed
    if service.daemon is not None:
        with settings(hide("running"), warn_only=True):
            exit_code = sudo("test -f /etc/init.d/{0}".format(service.daemon), shell=False).return_code
        if exit_code == 0:
            sudo("service {0} stop".format(service.daemon), shell=False)

@task
def check(service=None):
    """
    Check state of the Tallygram service.
    """
    if service.daemon is not None:
        sudo("service {0} status".format(service.daemon), shell=False)

@task
def restart(service=None):
    """
    Restarts the Tallygram service.
    """
    if service.daemon is not None:
        sudo("service {0} restart".format(service.daemon), shell=False)

@task
def install_binaries(service=None):
    """
    Install binaries and other files which are not depend from environment.
    """
    # Find last created package
    package = local('ls -t1 {0} | head -n1'.format(ENV.workspace + '/tallygram-backend-*.rpm'), capture=True)
    rpm = os.path.basename(package)
    remote_temp = ACTIONS.remote_tmp
    # Copy a RPM package to target host and install it
    run('rm -rf {0} && mkdir -p {0}'.format(remote_temp))
    put(package, remote_temp)
    sudo('yum -y -q localinstall {0}/{1} --cacheonly'.format(remote_temp, rpm), shell=False)

@task
def install(service=None):
    """
    Move distributable package and installs frontend on the target server
    """
    # Deploy binary files, create log directory, copy System V scripts
    install_binaries()
    # Upload property files on target machine
    configure(service)
#    configure_defaults(service)

@task
def getlogs(service=None):
    """
    Move distributable package and installs frontend on the target server
    """
    pass


def configure(service):
    """
    Upload configuration files on target host
    """
    print 'Tallygram'
    local_temp = os.path.join(ENV.workspace, env.host)
    if os.path.isdir(local_temp):
        remote_temp = ACTIONS.remote_tmp
        run('rm -rf {0}/{1} && mkdir -p {0}/{1}'.format(remote_temp, env.host))
        put(local_temp, remote_temp)
        run('rm -f {0}/*.properties'.format(service.home), shell=False)
        run('cp -p {0}/{1}/*.properties {2}'.format(remote_temp, env.host, service.home), shell=False)
        shutil.rmtree(local_temp)
    else:
        print 'Configuration files are not found!'

def configure_defaults(service):
    """
    Prepare configuration files for ARI service
    """
    return
    # Extract configuration files from last package
    with lcd(ENV.workspace):
        local('rpm2cpio $(ls -t1 {0} | head -n1) | cpio -idmu --quiet *.default'.format('tallygram-backend-*.rpm'))
    context={'unit' : service.settings()}

    # Process common templates and push it on target server
    for item in service.configs:
        process_template(item, context)


def process_template(template, context, idx=''):
    """
    Process a template of configuration file for Tallygram's backend
    """
    tmpdir = os.path.join(env.workspace, env.host)

    # Add index to a file name
    tmplname = template
    if idx != '':
        tmplname, tmplext = os.path.splitext(template)
        tmplname = tmplname + str(idx) + tmplext

    # Create a temporary directory to store configuration files
    if not os.path.exists(tmpdir):
        os.mkdir(tmpdir)

    # Process template
    cfgname = os.path.join(tmpdir, tmplname)
    engine = tenjin.Engine(path=ENV.templates_home_path())
    with open(cfgname, 'w') as f:
        print >> f, engine.render(template, context)

