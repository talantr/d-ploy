# -*- coding: utf-8 -*-
import os
import copy
import tenjin
import shutil
from tenjin.helpers import *
from fabric.context_managers import hide
from fabric.api import env, task, run, sudo, local,  cd, settings

# Pattern for tar.gz package
PKG="tallygram-ui-*.tar.gz"

tenjin.Engine.cache = tenjin.MemoryCacheStorage()

@task
def check():
    """
    Check state of the uWSGI and Nginx servers.
    """
    web_srv = env.product.elements['http-server']
    app_srv = env.product.elements['uwsgi']
    sudo("service {0} status".format(app_srv.daemon), shell=False)
    sudo("service {0} status".format(web_srv.daemon), shell=False)

@task
def install_binaries():
    """
    Move distributable package and installs frontend on the target server
    """
    # Find last created package
    with hide('running'):
        package = local('ls -t1 {0} | head -n1'.format(env.workspace + '/' + PKG), capture=True)
    print 'DEPLOY BINARY AND STATIC FILES...'
    # Copy a tar.gz package to target host and unpack it
    ACTION.upload(package)
    with cd(ACTION.remote_tmp):
        run('find . -mindepth 1 -maxdepth 1 -type d -exec rm -rf {} \;')
        run('tar -zmxf $(ls -t1 {0} | head -n1)'.format(PKG))
        run('mv ui/* .')

    # Replace old code by new one
    ACTION.rm('env', 'errors', 'apps-available', 'nginx')
    ACTION.push('env', 'errors', 'apps-available', 'nginx', '')


@task
def install():
    """
    Move distributable package and installs frontend on the target server
    """

    # Backup old binaries
    backup()

    print 'REMOVE OLD BINARY AND LOG FILES ...'
    # Remove old installation
    action.rm('logs/*', '')

    # Deploy binary files, create log directory
    install_binaries()

    # Actualize configuration files
    print 'UPLOAD INI FILES...'
    ACTION.upload(env.host + '/*.ini')
    ACTION.push(env.host + '/*.ini')

    print 'PUBLISH UWSGI APPLICATIONS...'
    uwsgi = env.product['frontend']['uwsgi']
    sudo('/bin/ln -sf -T {0} {1}/logs'.format(uwsgi.logdir, UNIT.home), shell=False)
    sudo('/bin/ln -sf {0}/apps-available/*.xml {1}/apps-enabled/'.format(UNIT.home, uwsgi.home), shell=False)

    print 'UPLOAD NGINX FILES...'
    ACTION.upload(env.host + '/admin.conf', env.host + '/public.conf')
    ACTION.push(env.host + '/*.conf',  'nginx/sites-available')

    print 'PUBLISH NGINX FILES...'
    nginx = env.product['frontend']['http-server']
    sudo('/bin/cp {0}/nginx/uwsgi_params {1}'.format(UNIT.home, nginx.home), shell=False)
    sudo('/bin/cp {0}/nginx/sites-templates/acl.cfg {1}'.format(UNIT.home, nginx.home), shell=False)
    sudo('/bin/ln -sf {0}/nginx/sites-available/*.conf {1}/conf.d'.format(UNIT.home, nginx.home), shell=False)
    sudo('/bin/mkdir -p {0}/ssl'.format(nginx.home), shell=False)
    sudo('/bin/ln -sf {0}/nginx/ssl/nginx.crt {1}/ssl/tallygram.crt.namecheap'.format(UNIT.home, nginx.home), shell=False)
    sudo('/bin/ln -sf {0}/nginx/ssl/nginx.key {1}/ssl/tallygram.key'.format(UNIT.home, nginx.home), shell=False)

    shutil.rmtree(os.path.join(env.workspace, env.host))

@task
def configure():
    """
    Prepare configuration files for frontend
    """
    # Get index of current frontend
    unit = env.product['frontend'][UNIT]
    feidx = list(set(x.hostname for x in unit.endpoints)).index(env.host)

    # Choose backed for current frontend
    context = get_user_context(feidx)

    # Process templates
    for item in UNIT.configurations:
        process_template(item, context)


@task
def backup():
    """
    Backup old binaries. Please, use before installation of new version
    """
    print 'BACKUP OLD FILES...'
    with settings(hide('warnings', 'running', 'stderr', 'stdout'), warn_only=True):
        rc = run('ls -l {0} 2>/dev/null'.format(UNIT.home))
    if rc:
        run('tar -czf backup-tallygram-ui-$(date +"%Y%m%d").tar.gz {0}'.format(UNIT.home), shell=False)
    else:
        print 'Target directory not found!'


def process_template(template, context, idx=''):
    """
    Process a template of configuration file for Tallygram's backend
    """
    tmpdir = os.path.join(env.workspace, env.host)

    # Add index to a file name
    tmplname = template
    if idx != '':
        tmplname, tmplext = os.path.splitext(template)
        tmplname = tmplname + str(idx) + tmplext

    # Create a temporary directory to store configuration files
    if not os.path.exists(tmpdir):
        os.mkdir(tmpdir)

    # Process template
    cfgname = os.path.join(tmpdir, tmplname)
    engine = tenjin.Engine(path=ENV.templates_home_path())
    with open(cfgname, 'w') as f:
        print >> f, engine.render(template, context)

def get_user_context(idx):
    context = {'user' : None, 'product' : env.product, 'environment' : env.product.environment}
    role = env.product['backend']
    uris = role['user-service'].endpoints
    usr_idx = idx % len(uris)
    context['user'] = copy.deepcopy(uris[usr_idx])
    context['user'].idx = usr_idx
    if context['user'].hostname == env.host:
        context['user'].hostname = 'localhost'
    return context
