# -*- coding: utf-8 -*-
from runner import job, Target
from fabric.api import sudo, run, puts

@job(Target.HOST)
def prepare(host):
    """
    Install general Thumbtack packages for deployment purposes.
    """
    puts("Installing Thumbtack packages for deployment purposes...")
    if ENV.os.lower() == 'debian':
        sudo('apt-get -y install lsb-release', shell=False)
        run('wget -q -O- http://repo.dev.thumbtack.net/debian/thumbtack.key | sudo apt-key add -', shell=False)
        run('echo "deb http://repo.dev.thumbtack.net/debian/ $(lsb_release -cs) thumbtack" | sudo tee /etc/apt/sources.list.d/thumbtack.list', shell=False)
        sudo("apt-get update -q -o Dir::Etc::sourcelist='/etc/apt/sources.list.d/thumbtack.list' -o Dir::Etc::sourceparts='-' -o APT::Get::List-Cleanup='0'", shell=False)
        sudo("apt-get -y install thumbtack-commons", shell=False)
    elif ENV.os.lower() == 'centos':
        run("sudo rpm -q thumbtack-release || sudo rpm -Uvh http://repo.dev.thumbtack.net/centos/thumbtack-release-0.1.1-1.noarch.rpm", shell=False)
        sudo("yum -y install thumbtack-commons", shell=False)
