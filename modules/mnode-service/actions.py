# -*- coding: utf-8 -*-
import os
import sys
import copy
import tenjin
import shutil
from tenjin.helpers import *
from fabric.api import env, task, run, sudo, put, local, lcd, cd

# Pattern for tar.gz package
PKG="tallygram-backend-*.rpm"

# Set cache for template processor
tenjin.Engine.cache = tenjin.MemoryCacheStorage()

class FakeObj(object):
    """
    Helper object
    """
    pass

@task
def configure(service=None):
    """
    Prepare configuration files for backend
    """
    # Extract configuration files from last package
    with lcd(ENV.workspace):
        local('rpm2cpio $(ls -t1 {0} | head -n1) | cpio -idmu --quiet *.properties'.format(PKG))

    srvloc = service.localize()
    srvloc.islocal = 'true'

    context={'dbs' : [], 'product' : PRODUCT, 'mnode' : srvloc}
    process_template('match_node.properties', context, srvloc.index)

    # Setup ARI context
    context.update(get_ari_context(service))
    context.update(get_match_context(service))

    # Process common templates and push it on target server
    for item in service.configs:
        process_template(item, context)

def process_template(template, context, idx=''):
    """
    Process a template of configuration file for Tallygram's backend
    """
    tmpdir = os.path.join(env.workspace, env.host)

    # Add index to a file name
    tmplname = template
    if idx != '':
        tmplname, tmplext = os.path.splitext(template)
        tmplname = tmplname + str(idx) + tmplext

    # Create a temporary directory to store configuration files
    if not os.path.exists(tmpdir):
        os.mkdir(tmpdir)

    # Process template
    cfgname = os.path.join(tmpdir, tmplname)
    engine = tenjin.Engine(path=ENV.template_locations())
    with open(cfgname, 'w') as f:
        print >> f, engine.render(template, context)

def get_ari_context(service):
    context = {'ari' : None, 'product' : env.product}
    # Setup ARI properties, only one instance of cache should be
    unit = PRODUCT['backend-ari'].ari
    for srv in unit.endpoints:
        if srv.hostname == service.hostname:
            context['ari'] = srv.localize()
            context['ari'].islocal  = 'true'
        else:
            context['ari'] = srv
            context['ari'].islocal = 'false'
        break #only one ARI is expected
    return context

def get_match_context(service):
    """
    Choose a match service for this match node, and create context
    All parameters for a match service are got from YAML file excepting hostname.
    """
    context = {'match' : None, 'mnode' : FakeObj(), 'product' : PRODUCT}

    unit = PRODUCT.backend.match
    i = service.index % len(unit.endpoints)
    srv = unit.endpoints[i]
    if srv.hostname == env.host:
        context['match'] = srv.localize()
        context['match'].islocal  = 'true'
    else:
        context['match'] = srv
        context['match'].islocal  = 'false'
    context['mnode'].count = service.count
    return context


def get_match_node_context(service):
    # Choose friend node settings
    context = {'mnode' : None, 'product' : env.product}
    for srv in service.module.endpoints:
        if srv.hostname == env.host:
            context['mnode'] = srv.localize()
            context['mnode'].islocal = 'true'
        else:
            context['mnode'] = srv
            context['mnode'].islocal = 'false'
        yield context

