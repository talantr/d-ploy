# -*- coding: utf-8 -*-
import sys
from fabric.api import env, task, sudo

@task
def start(service=None):
    """
    Starts the Mail server.
    """
    if service.daemon is not None:
        sudo("service {0} start".format(service.daemon), shell=False)

@task
def stop(service=None):
    """
    Stops the Mail server.
    """
    # Check that service was installed
    if service.daemon is not None:
        with settings(hide("running"), warn_only=True):
            exit_code = sudo("test -f /etc/init.d/{0}".format(service.daemon), shell=False).return_code
        if exit_code == 0:
            sudo("service {0} stop".format(service.daemon), shell=False)

@task
def check(service=None):
    """
    Check state of the Mail server.
    """
    if service.daemon is not None:
        sudo("service {0} status".format(service.daemon), shell=False)
