# -*- coding: utf-8 -*-
import os
import sys
import copy
import tenjin
import shutil
from tenjin.helpers import *
from fabric.api import env, task, run, sudo, put, local, lcd, cd

# Pattern for tar.gz package
PKG="tallygram-backend-*.rpm"


# Set cache of template processor
tenjin.Engine.cache = tenjin.MemoryCacheStorage()

@task
def configure(service):
    """
    Prepare configuration files for ARI service
    """
    print service.index, service.name
    # Extract configuration files from last package
    with lcd(ENV.workspace):
        local('rpm2cpio $(ls -t1 {0} | head -n1) | cpio -idmu --quiet *.properties'.format(PKG))

    user = service.localize()
    user.islocal  = 'true'
    TEMPLATOR.update_ctx(user = user, mnode = PRODUCT['match-node'].mnode)

    # Setup ARI context
    TEMPLATOR.update_ctx(ari = get_ari(service))

    # Setup cache context
    TEMPLATOR.update_ctx(cache = get_cache(service))

    # Choose database shard for friend node service
    TEMPLATOR.update_ctx(databases = get_databases(service))

    # Add a friend service in the context
    friend = get_friend_service(service)
    TEMPLATOR.update_ctx(friend = friend)

    # Add a match service in the context
    match = get_match_service(service)
    TEMPLATOR.update_ctx(match = match)

    message, mta = get_message_context(service)
    notification = get_notification_service(service)
    question = get_question_service(service)
    offline  = get_offline_service(service)
    TEMPLATOR.update_ctx(message = message, mta = mta, notification = notification, question = question, offline = offline)

    # Process common templates and push it on target server
    TEMPLATOR.prepare(service)
    TEMPLATOR.prepare(friend)
    TEMPLATOR.prepare(match)
    TEMPLATOR.prepare(message)
    TEMPLATOR.prepare(notification)
    TEMPLATOR.prepare(question)
    TEMPLATOR.prepare(offline)

    uyiuyuiy.uyguytfty

    # Process templates of configs for match nodes and push it on target server
    for ctx in get_match_node_context():
        process_template('match_node.properties', ctx, ctx['mnode'].index)

def process_template(template, context, idx=''):
    """
    Process a template of configuration file for Tallygram's backend
    """
    tmpdir = os.path.join(ENV.workspace, env.host)

    # Add index to a file name
    tmplname = template
    if idx != '':
        tmplname, tmplext = os.path.splitext(template)
        tmplname = tmplname + str(idx) + tmplext

    # Create a temporary directory to store configuration files
    if not os.path.exists(tmpdir):
        os.mkdir(tmpdir)

    # Process template
    cfgname = os.path.join(tmpdir, tmplname)
    engine = tenjin.Engine(path=ENV.template_locations())
    with open(cfgname, 'w') as f:
        print >> f, engine.render(template, context)

def get_ari(service):
    # Setup ARI properties, only one instance of cache should be
    for srv in PRODUCT['backend-ari'].ari.endpoints:
        if srv.hostname == service.hostname:
            srv = srv.localize()
            srv.islocal  = 'true'
        else:
            srv.islocal = 'false'
        break #only one ARI is expected
    return srv

def get_cache(service):
    cache = Context(hostname='127.0.0.1', port=11211, embeded='true')
    # Setup memcache properties
    for srv in PRODUCT['backend'].cache_server.endpoints:
        cache = srv.localize() if srv.hostname == service.hostname else srv
        cache.set('embeded', 'false')
        break #only one cache instance is expected
    return cache

def get_databases(service):
    """
    Get list of all endpoints for db shards.
    """
    dbs = []
    for srv in PRODUCT['mysql'].db_server.endpoints:
        dbs.append(srv.localize() if srv.hostname == service.hostname else srv)
    return dbs

def get_friend_service(service):
    """
    Generates conetxts for a friend service.
    All parameters for a friend service are got from YAML file excepting hostname.
    """
    for srv in PRODUCT['backend'].friend.endpoints:
        if srv.hostname == service.hostname:
            srv = srv.localize()
            srv.islocal  = 'true'
            return srv # For now, only one friend service can be run for a Java process

def get_match_service(service):
    """
    Generates conetxts for a match service.
    All parameters for a match service are got from YAML file excepting hostname.
    """
    match = Context()
    for srv in PRODUCT['backend'].match.endpoints:
        if srv.hostname == service.hostname:
            match = srv.localize()
            match.islocal  = 'true'
    return match

def get_message_service(service):
    """
    Generates conetxts for all message services.
    The method chooses MTA settings, other parameters are set in default values
    """
    # Choose MTA for a message service
    mta = PRODUCT['mta'].mail_server
    mta = mta.endpoints[service.index % mta.count]

    # Generates context for message services
    message = Context()
    for srv in PRODUCT['backend'].message.endpoints:
        if srv.hostname == service.hostname:
            message = srv.localize()
            message.islocal  = 'true'
    return message, mta

def get_match_node_context():
    """
    Generates conetxts for a match node services.
    All parameters for the service are got from YAML file.
    """
    context = {'mnode' : None, 'product' : env.product}
    unit = env.product['match-node'].mnode
    for srv in unit.endpoints:
        if srv.hostname == env.host:
            context['mnode'] = srv.localize()
            context['mnode'].islocal = 'true'
        else:
            context['mnode'] = srv
            context['mnode'].islocal = 'false'
        yield context

def get_notification_service(service):
    """
    Generates conetxts for a notification service.
    All parameters for the service are got from YAML file excepting hostname.
    """
    notify = Context()
    for srv in PRODUCT['backend'].notification.endpoints:
        if srv.hostname == service.hostname:
            notify = srv.localize()
            notify.islocal  = 'true'
    return notify

def get_question_service(service):
    """
    Generates conetxts for a question service.
    All parameters for the service are got from YAML file excepting hostname.
    """
    question = Context()
    for srv in PRODUCT['backend'].question.endpoints:
        if srv.hostname == service.hostname:
            question = srv.localize()
            question.islocal  = 'true'
    return question

def get_offline_service(service):
    """
    Find a offline service on the same host as for user service.
    """
    offline = Context()
    for srv in PRODUCT['backend'].offline.endpoints:
        if srv.hostname == service.hostname:
            offline = srv.localize()
            offline.islocal = 'true'
    return offline
