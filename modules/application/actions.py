# -*- coding: utf-8 -*-
from runner import job, Target
from fabric.api import puts, run, hide


@job(Target.HOST)
def test(host=None):
    """
    Starts the Nginx server.
    """
    puts("App. Test the host: {0}-{1} ({2})...".format(host.os, host.version, host.architecture))


@job(Target.HOST)
def describe(host):
    """
    Starts the Nginx server.
    """
    with hide('running', 'stdout'):
        platform = run('python -c "import platform; print platform.system(), platform.machine()" || uname -s && arch')
        family, cpu_arch, = platform.split()[:2]
        if family == 'Linux':
            os_info = eval(run('python -c "import platform; print platform.linux_distribution()"'))
            print os_info[0], cpu_arch
        elif family == 'Windows':
            os_info = eval(run('python -c "import platform; print platform.win32_ver()"'))
            print os_info[0], cpu_arch
        else:
            raise Exception('Unsupported OS type: %s' % family)


@job(Target.LOCAL)
def build():
    """
    This job creates a tarball with source code of the Enroute website to be able install it using python setup tools.
    """
    MODULE.version
    run('export $BUILDSPACE=/tmp/enroute-website WEBSITE_VERSION=0.1', shell=False)
    run('mkdir -p $BUILDSPACE', shell=False)
    run('cd $WORKSPACE/flask && tar -czf $BUILDSPACE/enroute-web-${WEBSITE_VERSION}.tar.gz . && cd - >/dev/null')




@job(Target.HOST)
def install(host):
    """
    Install from scratch or upgrade the Enroute website.
    """
    puts("Installing Enroute website {0}...".format(MODULE.version))
    # Copy a tarballon the target host
    sudo("virtualenv {0}".format(MODULE.home))
