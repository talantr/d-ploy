import os
import sys
from setuptools import setup, find_packages
from ploy import __version__


def get_cfg_path():
    if os.path.isdir(os.path.join('/etc')):
        return os.path.join('/etc')
    else:
        return 'bin'
    return os.path.join(sys.exec_prefix, 'lib', 'python%i.%i' % (sys.version_info[:2]), 'site-packages')

setup(name='d-ploy',
      version=__version__,
      description='Yet another configuration tool in Python',
      url='https://bitbucket.org/talantr/d-ploy',
      author='Anton Talevnin',
      author_email='talantr@gmail.com',
      packages=find_packages(exclude=['tests*']),
      data_files=[(get_cfg_path(), ['etc/d-ploy.cfg']),
                  ('usr/share/d-ploy/repo', [])],
      entry_points={
          'console_scripts': ['ploy = ploy.starter:main', ]
      },
      install_requires=['docopt>=0.6.1', 'fabric>=1.5.0', 'pyaml>=3.09', 'jinja2'],
      platforms="Any")
