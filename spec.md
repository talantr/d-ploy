# Терминология

Продукт (система) - это совокупность серверов и программного обеспечения (ПО). 

Модуль - независимо поставляемая единица ПО, т.е имеется возможность независимой установки 
бинарных или иных файлов, составляющих это ПО.

Роль - совокупность логически объединенных модулей представляющих какую-либо часть продукта.

Задача (работа) - набор инструкций, выполняемых на удаленном сервере, для достижения необходимого состояния 
определенного модуля системы. Каждая из работ выполняется на одном или нескольких серверах, используемых системой.

Ресурс - сервис ...

Примеры:

Продукт - реализация сайта на Python;
Элементы ПО - website, uwsgi, nginx, mysql;
Роли - site (website, uwsgi, nginx), db-server (mysql)
Работы: рестарт nginx, установка uwsgi на сервер.
Ресурсы: http://website00.example.com, http://website01.example.com


1. Фильтруем роли и модули в соответсвии с запросом пользователя;
  - учитываем зависимости модулей;
2. Импортируем задачи для необходимых модулей;
3. Проходим по списку ролей с учетом зависимостей и выполняем необходимую задачу

Проблема: учета зависимостей, 

Если добавлять зависимости в общий список, то теряется связь с хостами на которых необходимо выполнить задачи


# Цели

1. Реализовать описание продукта в YAML формате;
2. Реализовать импорт задач из Python файлов;
3. Каждая задача связана с элементом ПО;
4. Фильтр исполняемых задач по ролям, элементам и серверам; 

# Схема
1. Восстанавливаем продукт из Yaml описания (product).
2. Импортируем задачи, устанавливая соответсвие с элементами ПО.
3. Исполнение задач для кажого из ресурсов.

# Проблемы

1. На какой стадии реализовать фильтрацию?
 - может импортировать задачи и сложить их к элементам (тогда проблема передачи нужных аргуметов нужно решать иначе).
2. Алгоритм и аргументы. Формирование агруметнов для того или иного типа задач.
3. Bootstrap. Может происходить по инициативе сторонних сервисов. Как об этом узнать?
4. Проблема с поддержкой pyyaml 3.11, надо проверить
