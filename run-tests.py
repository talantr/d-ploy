#!/tmp/0/bin/python
# -*- coding: utf-8
"""
This python script run all or specified tests for D-Ploy project.

Requirements:

- Python 2.6+;
- nose 1.3.0;
- unittest2
"""

import nose
import os
import sys
import ConfigParser

my_path, my_name = os.path.split(os.path.realpath(__file__))


def get_arguments(args):
    """
    Parses command line arguments and return the result of parsing
    """
    from argparse import ArgumentParser
    parser = ArgumentParser(description="Run all or specified integrated tests for Marin Pull Report Service")
    parser.add_argument("--version", action="version", version="%(prog)s 1.0")
    tests = parser.add_argument_group("tests")
    tests.add_argument("-c", "--config", type=str, dest="config", default=my_path + "/test.cfg",
                       help="a configuration file where an environment is described")
    tests.add_argument("-a", "--test-action", dest="action", default = None, action='append',
                       help="Run the specified type of tests [insert, delete, update]")
    tests.add_argument("-l", "--test-location", type=str, dest="location", default=os.path.join(os.getcwd(),""),
                       help="a directory where tests are placed")
    tests.add_argument("--xunit-file", type=str, dest="report",
                       default=os.path.join(os.getcwd(), "d-ploy-test-report.xml"),
                       help="Path to xml file to store the xunit report in")
    return parser.parse_known_args(args)[0]


def main(args):
    """
    Read a configuration file, set up connection settings for MySQL and Aerospike and run required tests.
    """
    actions = map(lambda x: 'action=' + x, args.action) if args.action else None
    nose.main(env={'NOSE_WHERE': args.location, 'NOSE_NOCAPTURE': True, 'NOSE_INCLUDE_EXE': True,
                   'NOSE_WITH_XUNIT': True, 'NOSE_XUNIT_FILE': args.report, 'NOSE_ATTR': actions})

if __name__ == "__main__":
    main(get_arguments(sys.argv[1:]))
