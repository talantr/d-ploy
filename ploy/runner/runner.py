from Queue import Queue, Empty, Full
from threading import Thread
from fabric.api import execute, settings, hide


class PoolUsageError(Exception):
    """
    Incorrect usage the pool by calling program
    """
    pass


class JobRunnerPool(object):
    """
    A pool of workers to perform tasks on remote hosts in parallel.
    """
    RUNNING = 0x01
    STOPPED = 0x02
    INTERRUPTED = 0x04

    def __init__(self, size=4):
        """
        Initialize a pool with given number of workers.
        """
        #todo: how to choose maxsize for the pool
        self._jobs = Queue(maxsize=1024+2*size)
        self._pool_size = size
        self._workers = []
        self._is_active = False
        self._status = JobRunnerPool.STOPPED
        self._sleep_time = 0.75
        self._stderr = []

    def start(self):
        """
        Start or restart all workers in the pool.
        """
        if self._is_active:
            raise PoolUsageError("The pool is running, you can start it after all jobs will be completed")
        self._is_active = True
        self._status = JobRunnerPool.RUNNING
        workers = map(lambda i: Thread(name='D-ploy Worker-%i' % i, target=self.processor), xrange(self._pool_size))
        self._workers = workers
        for w in workers:
            w.daemon = True
            w.start()

    def add(self, *jobs):
        """
        Add one or few tasks in the pool. You have to start the pool before you will invoke this method,
        otherwise a PoolUsageError exception will be thrown.
        """
        if not self._is_active:
            raise PoolUsageError("You should start the pool before adding tasks.")
        for job in jobs:
            while self._is_active:
                try:
                    self._jobs.put(job, block=True, timeout=self._sleep_time)
                    break
                except Full:
                    pass

    def wait(self):
        """
        Wait while all jobs in the queue will be performed.
        """
        if self.is_running():
            self._jobs.join()
            self._is_active = False
            for w in self._workers:
                w.join(self._sleep_time)
            self._status = JobRunnerPool.STOPPED
        return self._stderr

    def interrupt(self):
        """
        Stop task processing in this pool.
        """
        self._jobs.mutex.acquire()
        self._is_active = False
        self._status = JobRunnerPool.INTERRUPTED
        self._jobs.queue.clear()
        self._jobs.all_tasks_done.notify_all()
        self._jobs.unfinished_tasks = 0
        self._jobs.mutex.release()

    def processor(self):
        """
        The main loop of each worker in this pool, it gets a task from queue and try to execute it.
        """
        job = None
        while self._is_active:
            try:
                job = self._jobs.get(block=True, timeout=self._sleep_time)
                if job.log_level:
                    execute(job, host=job.host.hostname)
                else:
                    with settings(hide('running')):
                        execute(job, host=job.host.hostname)
                self._jobs.task_done()
            except Empty:
                pass
            except Exception as e:
                self._jobs.task_done()
                self._stderr.append((job, e))
            except BaseException as e:
                self.interrupt()
                raise e

    def is_running(self):
        """
        Determine whether the pool is running.
        """
        return self._status == JobRunnerPool.RUNNING
