from ploy.models.entities.resource import LOCALHOST


class JobFilter(object):
    """
    This is a filter for running jobs. It helps to filter jobs by roles, modules or hosts.
    """

    __slots__ = ('task', 'roles', 'modules', 'hosts', '_product')

    def __init__(self, product, roles=None, modules=None, hosts=None):
        """
        Creates a filter for given collections of roles, modules and hosts.
        The filter will return True only for jobs which have these roles, modules and hosts.
        """
        # self._product = product
        self.roles = set(product[r] for r in roles or product)
        self.modules = self._select_modules(modules)
        self.hosts = self._select_hosts(hosts)

    def __call__(self, job):
        """
        Check whether the given job is specified for a triple of (module, role, host) from this filter.
        """
        return job.module in self.modules and job.host in self.hosts

    def _select_modules(self, modules):
        mods = set()
        if not modules:
            for role in self.roles:
                for m in role.modules():
                    mods.update(r for r in m.requirements)
                    mods.add(m)
        else:
            for role in self.roles:
                for m in role.modules():
                    if m.name in modules:
                        mods.update(r for r in m.requirements)
                        mods.add(m)
        return mods

    def _select_hosts(self, hosts):
        """
        Converts the given list of host names or IP-addresses into a set of Host instances.
        If the `hosts` parameter is None then a set of all hosts by all roles in the filter is returned.
        """
        host_set = set()
        if not hosts:
            host_set.add(LOCALHOST)
            for r in self.roles:
                for m in r.modules():
                    host_set.update(r.hosts(m))
        else:
            for r in self.roles:
                host_set.update(filter(lambda h: h.hostname in hosts, r.hosts()))
        return host_set
