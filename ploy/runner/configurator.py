"""
This module contains implementation of a parser for main d-ploy configuration file.
"""
from .repository import LocalFileRepo
from ploy.models.jobs.task import JobTargetType
from yaml import load as yaml_load
try:
    from yaml import CLoader as YamlLoader
except ImportError:
    from yaml import Loader as YamlLoader


class JobEntry(object):
    """
    A job entry in the flow definition
    """

    TARGET_TYPE_MAP = {'host': JobTargetType.HOST, 'resource': JobTargetType.RESOURCE, 'localhost': JobTargetType.LOCAL}

    __slots__ = ('_name', '_target', '_on_success', '_on_fail')

    def __init__(self, name, target=JobTargetType.HOST, success=None, fail=None):
        self._name = name
        self._target = target
        self._on_success = success
        self._on_fail = fail

    @property
    def name(self):
        return self._name

    @property
    def target(self):
        return self._target

    @property
    def on_success(self):
        return self._on_success

    @property
    def on_fail(self):
        return self._on_fail

    def __repr__(self):
        return '<JobEntry: {0}>'.format(self._name)


class YamlContextConfigurator(object):
    """
    This class provides global settings for D-ploy tool from YAML configuration file.
    """

    __slots__ = ('_repo_list', '_context', '_jobs')

    # Description of keys which can be used in a YAML configuration.
    REPO_KEY = 'repositories'
    FLOW_KEY = 'job-flow'

    DEFAULT_REPO_PATH = '/var/d-ploy/repo'

    def __init__(self, context, cfg_file_path):
        """
        Parse
        """
        self._repo_list = []
        self._context = context
        self._jobs = []
        with file(cfg_file_path, 'r') as cfg_stream:
            cfg_map = yaml_load(cfg_stream, Loader=YamlLoader)
            self._create_repositories(cfg_map)
            self._create_flow(cfg_map)

    def repositories(self):
        """
        Gets enumeration of all repositories.
        """
        return self._repo_list

    def jobs(self):
        """
        Gets enumeration of all repositories.
        """
        return self._jobs

    def _create_repositories(self, cfg_map):
        """
        Creates repository instances from configuration file, adds the instances into internal repository list
        """
        repo_list = cfg_map.get(self.REPO_KEY, {'global': {'path': self.DEFAULT_REPO_PATH}})
        for repo in repo_list:
            for repo_name, args in repo.items():
                repo_type = args.pop('type', 'local-file-repo')
                if repo_type == 'local-file-repo':
                    self._repo_list.append(LocalFileRepo(self._context.product, **args))

    def _create_flow(self, cfg_map):
        """
        """
        entries = cfg_map.get(self.FLOW_KEY, [])
        for e in entries:
            for name, meta in e.iteritems():
                target = JobEntry.TARGET_TYPE_MAP[meta.get('target-type', 'host')]
                post_job = meta.get('post-job', {'success': 'break', 'fail': 'break'})
                self._jobs.append(JobEntry(name, target, success=post_job.get('success', 'break'),
                                           fail=post_job.get('fail', 'break')))
                # print name, meta

