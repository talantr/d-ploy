from ploy.models.entities.configurator import FlexConfig


class Context(FlexConfig):

    def reset(self, settings={}):
        self._settings = settings
        # Add read-only properties dynamically
        for k in settings:
            setattr(self.__class__, k.replace('-', '_'), self.create_property(k))
