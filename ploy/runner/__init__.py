from repository import JobRegistry
from filter import JobFilter
from runner import JobRunnerPool

__all__ = ['Target',
           'JobRegistry',
           'JobFilter']


class _Registry(object):
    """
    Global registry for running jobs. It's wrapper which contains an active JobRegistry instance.
    All implemented jobs in the local file repository are imported in active registry always.
    """

    jobs = JobRegistry()

    @staticmethod
    def register(*args, **kwargs):
        """
        Adds new job in the global registry.
        """
        return _Registry.jobs.register(*args, **kwargs)

registry = _Registry

# Use this wrapper to define D-Ploy jobs and add them to global registry.
job = _Registry.register


class UnsupportedPlatform(Exception):
    pass
