import glob
import logging
import os
from imp import new_module
from filter import JobFilter
from ploy.models import Target
from ploy.runner.template import create_template_processor
from ploy.models.jobs.task import ResourceGoal, HostGoal, DedicatedHostGoal


def regularize_goals(goals, resolved=set(), unresolved=set()):
    """
    Sort a collection of goals.
    """
    sorted_goals = []
    for g in goals:
        if g not in resolved and g not in unresolved:
            unresolved.add(g)
            for r in g.module.requirements:
                if r in unresolved:
                    raise Exception('Circular module dependencies detected')
                if r not in resolved:
                    sorted_goals.extend(regularize_goals(goals, resolved, unresolved))
            resolved.add(g)
            sorted_goals.append(g)
            unresolved.remove(g)
    return sorted_goals


class JobRegistry(object):
    """
    This is a global catalog of imported goals which can be used with the D-ploy utility.
    Only one instance of this class should be created by your application. There is a global
    variable in this package, all imported jobs will be registred here.
    """
    __slots__ = ('_goals', '_product', '_logger', '_gset')

    JOB_TYPES = {Target.LOCAL: DedicatedHostGoal,
                 Target.HOST: HostGoal,
                 Target.RESOURCE: ResourceGoal}

    def __init__(self, product=None):
        """
        Creates empty collection of jobs for given product.
        """
        self._logger = logging.getLogger('JobRegistry')
        self._logger.propagate = True
        self._product = product
        self._goals = []
        self._gset = set()

    def register(self, scope=Target.RESOURCE):
        """
        Add new job in this registry. This method is used as decorator to mark implemented jobs.
        """
        # If scope is undefined then it's resource
        if callable(scope):
            job = scope
            module = self._product.get_module(job.__module__)
            if module is not None:
                self.add_goal(self.create_goal(Target.RESOURCE, job, module,
                                               filter(lambda r: module in r, self._product)))
            return job
        else:
            def wrapper(*args):
                job = args[0]
                module = self._product.get_module(job.__module__)
                if module is not None:
                    self.add_goal(self.create_goal(scope, job, module, filter(lambda r: module in r, self._product)))
                return job
            return wrapper

    def add_goal(self, goal):
        """
        (module, target_type, task)
        """
        if goal not in self._gset:
            self._goals.append(goal)
            self._gset.add(goal)
            self._logger.info("The goal %s has been added in the global registry", goal)

    def create_goal(self, job_type, job, module, role):
        """
        Create a goal for specified module and role.
        """
        goal_type = self.JOB_TYPES[job_type]
        return goal_type(job, module, role)

    def __iter__(self):
        """
        Iterate by all jobs in this registry.
        """
        for goal in regularize_goals(self._goals, set(), set()):
            for job in goal:
                yield job


class LocalFileRepo(object):
    """
    This class provides a mechanism to import implementation of D-Ploy jobs from local files.
    """

    ID = 'local-file-repo'

    @staticmethod
    def import_tasks_from_file(module_name, path, loc={}):
        """
        Import a set of tasks from python file.

        The `module_name` is name of the creating module for tasks.

        The `path` is path to a file which contains implementation of tasks.
        """
        module = new_module(module_name)
        module.__file__ = path
        module.__dict__.update(loc)
        try:
            execfile(path, module.__dict__)
        except IOError, e:
            e.strerror = 'Unable to load a tasks (%s)' % e.strerror
            raise
        return module

    def __init__(self, product, path, file_mask='*.py'):
        self._location = path
        self._mask = file_mask
        self._product = product
        self._modules = []

    def import_tasks(self, sieve=None):
        """
        Import all fabric tasks for some product from sub-directories of specified directory.
        """
        if os.path.isdir(self._location):
            sieve = sieve or JobFilter(self._product)
            for module in sieve.modules:
                if os.path.isdir(self._location + '/' + module.name):
                    for job_file in glob.glob(self._location + '/' + module.name + '/' + self._mask):
                        #action = Action(u, product)
                        imported = {'MODULE': module,
                                    'ENV': self._product.environment,
                                    'PRODUCT': self._product,
                                    #'ACTIONS' : action,
                                    #'Context' : Context,
                                    'TEMPLATE': create_template_processor(self._product),
                        }
                        py_module = self.import_tasks_from_file(module.name, job_file, imported)
                        self._modules.append(py_module)

    def __repr__(self):
        """
        Gets string representation for the local file repository.
        """
        return '<%s: %s>' % (self.__class__.__name__, self._location)
