
def describe(ctx, registry):
    """
    Polls remote hosts to get information about their configuration.
    OS name and version, arch
    """
    pass


def jobs(ctx, registry):
    """
    Gets a list of all jobs which are satisfied the command line criteria
    """
    pass


def ping(ctx, registry):
    """
    Ping SSH port for all hosts which are satisfied the command line criteria
    """
    pass


def check_logon(ctx, registry):
    """
    Tries to log on remote hosts using provided credentials.
    """
    pass