import logging
import os
from jinja2 import Environment as JinjaEnvironment, FileSystemLoader, TemplateNotFound


class J2Engine(object):
    """
    A wrapper around jinja2 to add ability for processing configuration files.
    """

    __slots__ = ('_engine', '_context', '_product', '_env', 'logger')

    def __init__(self, product):
        """
        Initialize engine and set up paths to search for templates
        """
        self.logger = logging.getLogger('J2Engine')
        self._context = dict((r.name, r) for r in product)
        self._product = product
        self._env = product.environment
        self._engine = None
        # self._engine = JinjaEnvironment(loader=FileSystemLoader(map(lambda x: x, slefproduct.environment.template_locations())))

    def prepare(self, rsc):
        self.update_ctx(**{'this': rsc, 'product': self._product, 'environment': self._env})
        out_dir = os.path.join(self._env.buildspace, rsc.host.hostname, rsc.name)
        self._engine = JinjaEnvironment(loader=FileSystemLoader(map(lambda x: x, self._env.template_locations())))

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        for cfg_name in rsc.configs:
            cfg_file_name = os.path.split(cfg_name)[1]
            try:
                template = self._engine.get_template(cfg_file_name)
                cfg_file_name = os.path.join(out_dir, cfg_file_name)
                with open(cfg_file_name, 'w') as config:
                    print >> config, template.render(**self._context)
            except TemplateNotFound:
                pass # skip configuration file if their template is not found

    def update_ctx(self, **kwargs):
        self._context.update(kwargs)

    def prepare_one(self, rsc, cfg_name, target=None):
        self.update_ctx(**{'this': rsc, 'product': self._product, 'environment': self._env})
        out_dir = os.path.join(self._env.buildspace, rsc.host.hostname)

        if not os.path.exists(out_dir):
            os.mkdir(out_dir)

        cfg_file_name = os.path.split(cfg_name)[1]
        try:
            template = self._engine.get_template(cfg_file_name)
            cfg_file_name = os.path.join(out_dir, target if target else cfg_file_name)
            with open(cfg_file_name, 'w') as config:
                print >> config, template.render(**self._context)
        except TemplateNotFound:
            pass # skip configuration file if their template is not found

