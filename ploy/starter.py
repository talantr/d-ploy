#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage: ploy <job> [options]

Options:
-c <yaml-file> --config <yaml-file>            Description environment in YAML format [default: example.yaml];
-r <role name> --role <role name>              Name of a role jobs will be executed for;
-m <module[,...]> --module <module[,...]>      A software module jobs will be executed for;
-h <host[,...]> --hosts <host[,...]>           List of hosts where specified jobs will be run;

-w <workspace directory> --workspace           A path to working directory, where source code has been placed.

-l <user name> --login <user name>             Override the username from YAML definition to log on remote servers;
-i <key>
"""

#--dry-run
import logging
import os
import ploy
import signal
import sys
from ploy.api import Context, JobFilter
from ploy.models import DescribeGoal
from ploy.models.entities.parsers import YamlProductParser
from ploy.runner import registry, JobRegistry, JobRunnerPool
from ploy.runner.configurator import YamlContextConfigurator

from docopt import docopt
from fabric.api import env


def get_config_file():
    """
    Gets full path to the D-Ploy configuration file.
    """
    cfg_file = os.path.expanduser('~/d-ploy.cfg')
    if os.path.isfile(cfg_file):
        return cfg_file
    cfg_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'd-ploy.cfg')
    if os.path.isfile(cfg_file):
        return cfg_file
    return os.path.join('/etc', 'd-ploy.cfg')


def start(ctx, task_name, roles, modules, hosts):
    # Create a filter for jobs which will be executed;
    sieve = JobFilter(ctx.product, roles=roles, modules=modules, hosts=hosts)
    registry.jobs = JobRegistry(ctx.product)
    pool = JobRunnerPool(size=1)
    signal.signal(signal.SIGINT, pool.interrupt)
    pool.start()
    pool.add(*DescribeGoal(sieve.hosts))
    # Import all jobs from all declared repositories
    try:
        configurator = YamlContextConfigurator(ctx, get_config_file())
        for repo in configurator.repositories():
            repo.import_tasks(sieve)
    except:
        pool.interrupt()
        raise
    # Run imported jobs on target hosts
    if pool.is_running():
        for job in filter(sieve, registry.jobs):
            if job.name == task_name:
                pool.add(job)
    return pool.wait()


def main():
    out = []
    try:
        root_logger = logging.getLogger()
        console = logging.StreamHandler()
        root_logger.addHandler(console)
        args = docopt(__doc__, version="D-ploy %s" % ploy.__version__)
        parser = YamlProductParser(args.get('--config'), workspace=args.get('--workspace'))
        ploy.ctx = Context(product=parser.build_product())
        task_name = args.get('<job>')
        if args.get('--role') is not None:
            args['--role'] = args.get('--role').split(',')
        if args.get('--module') is not None:
            args['--module'] = args.get('--module').split(',')
        if args.get('--hosts') is not None:
            args['--hosts'] = set(args.get('--hosts').split(','))
        # initialize Fabric settings
        env.user = args['--login'] or ploy.ctx.product.environment.deployer.login
        env.password = ploy.ctx.product.environment.deployer.password
        if not env.password:
            env.key_filename = args['-i'] or ploy.ctx.product.environment.deployer.key
        out = start(ploy.ctx, task_name, args.get('--role'), args.get('--modules'), args.get('--hosts'))
        if len(out) > 0:
            print >> sys.stderr, '****************************************************'
            for rc in out:
                print >> sys.stderr, 'JOB: %s ERROR: %s' % (rc[0].name, rc[1])
    except Exception, ex:
        raise
        print >> sys.stderr, 'EX:', ex
        exit(2)
    except BaseException as ex:
        print >> sys.stderr, 'All actions have been canceled', ex
        exit(1)
    exit(0 if len(out) == 0 else 1)


if __name__ == '__main__':
    main()
