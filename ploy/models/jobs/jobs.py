# -*- coding: utf-8 -*-
"""
This module contains an implementation of internal D-Ploy jobs.
"""
import os
from fabric.api import hide, run, local


class UnsupportedPlatform(Exception):
    """
    Raise if there is no job to archive a goal for specific platform.
    """
    pass


def __cli_wrapper(host, cmd):
    if host.hostname in ('127.0.0.1', 'localhost'):
        return local(cmd, capture=True)
    return run(cmd, quiet=True)


def describe(host):
    """
    Gathers details/facts (OS name and version) about given host.
    """
    with hide('running', 'stdout'):
        platform = __cli_wrapper(host, 'python -c "import platform; print platform.system(), platform.machine()"'
                                       ' || uname -s && arch')
        family, cpu_arch, = platform.split()[:2]
        host._platform = family
        host._arch = cpu_arch
        if family == 'Linux':
            os_info = eval(__cli_wrapper(host, 'python -c "import platform; print platform.linux_distribution()"'))
            host._os = os_info[0].lower()
            host._version = os_info[1]
        elif family == 'Windows':
            os_info = eval(__cli_wrapper(host, 'python -c "import platform; print platform.win32_ver()"'))
            host._os = os_info[0]
            host._version = os_info[1]
        else:
            raise UnsupportedPlatform('Unsupported OS type: %s' % family)


class Action(object):

    def __init__(self, unit, product):
        self._unit = unit
        self._product = product
        self._remote_tmp = '~' + product.environment.deployer.login + '/' + unit.name

    def upload(self, *artefacts):
        for item in artefacts:
            path, name = os.path.split(item)
            if path.startswith(self.local_tmp):
                path = path[len(self.local_tmp):]
                if path and path[0] == '/': path = path[1:]
            local_tmp = os.path.join(self.local_tmp, path)
            remote_tmp = self.remote_tmp + "/" + path
            with hide('running'):
                run('/bin/mkdir -p ' + remote_tmp, shell=False)
            with lcd(local_tmp):
                put(name, remote_tmp)

    def download(self, artefact):
        print "download ........."
        return
        with cd(self._remote_tmp):
            get(artefact, self._local_tmp)

    def push(self, *artefacts):
        """
        Copies specified files or folders from temp directory into home directory.
        If you pass at least two parameters then the last of them is must be a target directory.
        """
        prefix = ''
        if len(artefacts) > 1:
            prefix = artefacts[-1]
            artefacts = artefacts[:-1]
        with settings(hide('warnings','running'), warn_only=True):
            sudo('/bin/mkdir -p ' + self._unit.home + '/' + prefix, shell=False)
        for target in artefacts:
            sudo('/bin/cp -prf %s/%s %s/%s' % (self.remote_tmp, target, self._unit.home, prefix), shell=False)

    def rm(self, *artefacts):
        """
        Remove specified files or folders in home directory of the unit on remote host.
        """
        target = ','.join(artefacts)
        sudo('/bin/rm -rf %s/{%s}' % (self._unit.home, target), shell=False)

    def pull(self, *artefacts):
        """
        Last argument is the target directory for pulling files.
        """
        prefix = ''
        if len(artefacts) > 1:
            prefix = artefacts[-1]
            artefacts = artefacts[:-1]
        with settings(hide('warnings','running'), warn_only=True):
            sudo('/bin/mkdir -p ' + self.remote_tmp + '/' + prefix, shell=False)
        for target in artefacts:
            sudo('/bin/cp -prf %s/%s %s/%s' % (self._unit.home, target, self.remote_tmp, prefix), shell=False)

    @property
    def local_tmp(self):
        return self._product.environment.buildspace

    @property
    def remote_tmp(self):
        with hide('running', 'stdout'):
            path = run('/bin/echo ' + self._remote_tmp, shell=False)
        return path

    @property
    def unit(self):
        """
        Gets unit object for the action
        """
        return self._unit

