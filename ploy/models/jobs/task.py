from .jobs import describe
from ploy.models.entities.module import DPloyModule
from ploy.models.entities.resource import LOCALHOST


def enum(**enums):
    return type('Enum', (), enums)

# Enumeration of possible exit codes for running jobs
ReturnCode = enum(SUCCESS=0, ERROR=1, FAIL=255)

# Enumeration of target types for jobs
JobTargetType = enum(LOCAL=0x01, HOST=0x02, RESOURCE=0x04)


class Task(object):
    """
    The class defines an action and location where the action should be executed.
    """

    __slots__ = ('_host', '_module', '_task', '_args', '_kwargs', 'retries', 'log_level')

    def __init__(self, task, host, module, *args, **kwargs):
        self._host = host
        self._module = module
        self._task = task
        self._args = args
        self._kwargs = kwargs
        self.retries = 0
        self.log_level = 1

    @property
    def host(self):
        """
        Gets a Host object which defines where this task will be executed.
        """
        return self._host

    @property
    def module(self):
        """
        Gets element of target system, this task will be executed for.
        """
        return self._module

    @property
    def name(self):
        """
        Gets string representation of the executed action
        """
        return self._task.__name__

    def __call__(self):
        """
        Run this task. Wraps callable object to run it without any arguments.
        """
        self._task(*self._args, **self._kwargs)

    def __repr__(self):
        return '<Task: {0}.{1} on {2}>'.format(self._module.id, self.name, self._host.hostname)


class Goal(object):
    """
    This is a base class for all goals which D-Ploy can be archive, e.g. it may be installation or configuration
    software. One goal may produce one or more tasks to archive this goal, for example, to upgrade your cluster
    you must install a package on several servers, so you the D-ploy need to run a task for each server (or may be
    service) to archive the 'upgrade' goal. Each module has own Goal instance.
    """

    __slots__ = ('_module', '_name', '_roles', '_context')

    def __init__(self, name, module, roles):
        self._name = name
        self._module = module
        self._roles = roles

    @property
    def name(self):
        """
        Gets the goal name
        """
        return self._name

    @property
    def module(self):
        """
        Gets software module which is associated with this goal.
        """
        return self._module

    @property
    def roles(self):
        """
        Gets a list of roles which is associated with this goal.
        """
        return self._roles

    def __iter__(self):
        """
        Generate all tasks which should be performed to archive this goal
        """
        if False:
            yield None

    def __eq__(self, other):
        return type(self) == type(other) and self._module.id == other.module.id and self._name == other.name

    def __hash__(self):
        return hash(self._module.id + self._name)

    def __repr__(self):
        """
        Gets string representation of the goal.
        """
        return '<Goal: {0}.{1}>'.format(self._module.id, self._name)


class DedicatedHostGoal(Goal):
    """
    The goal must be executed once on a server which is specified by user. If user didn't define the server address,
    the goal will be executed on the localhost.
    """
    __slots__ = ('_wrapper', '_host')

    def __init__(self, wrapper, module, role, host=LOCALHOST):
        super(DedicatedHostGoal, self).__init__(wrapper.__name__, module, role)
        self._host = host
        self._wrapper = wrapper

    def __iter__(self):
        """
        Retrieve a task that must be executed on the specified host, by default on the localhost.
        """
        yield Task(self._wrapper, self._host, self._module, self._host)


class ResourceGoal(Goal):
    """
    This is a wrapper for Fabric tasks.
    """

    __slots__ = '_wrapper'

    def __init__(self, wrapper, module, role):
        """
        Creates a generator of tasks for a callable wrapper and given list of resources.
        """
        super(ResourceGoal, self).__init__(wrapper.__name__, module, role)
        self._wrapper = wrapper

    def __iter__(self):
        for role in self._roles:
            for rsc in role[self._module]:
                yield Task(self._wrapper, rsc.host, self._module, rsc)


class HostGoal(ResourceGoal):
    """
    This class groups all resources by hosts and produce only one job per host.
    """

    __slots__ = ()

    def __iter__(self):
        """
        Enumerates jobs which must be executed once on each target host.
        """
        for r in self._roles:
            for h in r.hosts(self._module):
                yield Task(self._wrapper, h, self._module, h)


class DescribeGoal(Goal):
    """
    The meaning of this goal is to gather information about remote hosts, such as OS type, its version
    and CPU architecture. It may be necessary for choosing a steps how to archive other goals.
    """

    def __init__(self, hosts, cause=None):
        """
        Creates a generator of tasks for a callable wrapper and given list of resources.
        """
        self._hosts = hosts
        self._cause = cause
        super(DescribeGoal, self).__init__(':describe', DPloyModule, None)

    def __iter__(self):
        for host in self._hosts:
            t = Task(describe, host, self._module, host)
            t.log_level = 0
            yield t


# TODO: Seems this is a task, not goal, so we need to define the scope of this task
class _TerminateJob(Goal):
    """
    This is a built-in task to interrupt flow of tasks execution and process result of last executed task.
    """

    def __init__(self):
        super(_TerminateJob, self).__init__('terminate', None, None)

    def execute(self, task, rc):
        """
        Just interrupts program execution and returns a status of execution.
        """
        exit(rc)

terminate = _TerminateJob()
