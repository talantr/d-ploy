import os
from artefact import Artefact
from fabric.api import hide, lcd, run, put, settings, sudo
import ploy


class FileSet(Artefact):
    """
    The class provides user credentials for system deployment process
    """
    __slots__ = ('_files', '_module')

    @staticmethod
    def get_default_local_temp(module, hostname):
        return os.path.join(ploy.ctx.product.environment.buildspace, hostname, module.id)

    @staticmethod
    def get_default_remote_temp(module):
        return "~$(id -u -n)/" + module.id

    def __init__(self, module, *files):
        """
        Initialize deployer credentials. If login is None then it is used current local user name.
        """
        super(FileSet, self).__init__()
        self._files = files
        self._module = module

    def upload(self, host, local_temp=None, remote_temp=None):
        """
        Uploads files on the target host
        """
        local_temp = local_temp or self.get_default_local_temp(self._module, host.hostname)
        remote_temp = remote_temp or self.get_default_remote_temp(self._module)
        for item in self._files:
            path, name = os.path.split(item)
            if path.startswith(local_temp):
                path = path[len(local_temp):]
                if path and path[0] == '/':
                    path = path[1:]
            local_temp = os.path.join(local_temp, path)
            with hide('running', 'output'):
                remote_temp = run("eval echo " + remote_temp, shell=True)
                run('/bin/mkdir -p {0}/{1}'.format(remote_temp, path), shell=True)
            with lcd(local_temp):
                put(name, remote_temp + "/" + path)
        return self

    def download(self, host):
        raise Exception("The download method must be implemented")

    def push(self, *artefacts):
        """
        Copies specified files or folders from temp directory into home directory.
        If you pass at least two parameters then the last of them is must be a target directory.
        """
        prefix = ''
        if len(artefacts) > 1:
            prefix = artefacts[-1]
            artefacts = artefacts[:-1]
        remote_temp = self.get_default_remote_temp(self._module)
        with settings(hide('warnings', 'running', 'output'), warn_only=True):
            remote_temp = run("eval echo " + remote_temp, shell=True)
            sudo('/bin/mkdir -p ' + self._module.home + '/' + prefix, shell=False)
        for target in artefacts:
            sudo('/bin/cp -prf %s/%s %s/%s' % (remote_temp, target, self._module.home, prefix), shell=False)
        return self

    def rm(self, *artefacts):
        """
        Remove specified files or folders in home directory of the module on remote host.
        """
        target = ','.join(artefacts)
        sudo('/bin/rm -rf %s/{%s}' % (self._module.home, target), shell=False)

    def pull(self, *artefacts):
        """
        Last argument is the target directory for pulling files.
        """
        prefix = ''
        if len(artefacts) > 1:
            prefix = artefacts[-1]
            artefacts = artefacts[:-1]
        with settings(hide('warnings','running'), warn_only=True):
            sudo('/bin/mkdir -p ' + self.remote_tmp + '/' + prefix, shell=False)
        for target in artefacts:
            sudo('/bin/cp -prf %s/%s %s/%s' % (self._unit.home, target, self.remote_tmp, prefix), shell=False)
