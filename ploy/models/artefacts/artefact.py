
from os.path import expanduser
from getpass import getuser


class Artefact(object):

    __slots__ = ()


class Credential(Artefact):
    """
    The class provides user credentials for system deployment process
    """
    __slots__ = ('_username', '_password', '_key_path')

    def __init__(self, login=None, password=None, key_path=None):
        """
        Initialize deployer credentials. If login is None then it is used current local user name.
        """
        super(Artefact, self).__init__()
        self._username = login or getuser()
        self._password = password or ''
        self._key_path = key_path or expanduser('~{0}/.ssh/id_rsa'.format(self._username))

    @property
    def login(self):
        """
        Gets user name to log on remote host.
        """
        return self._username

    @property
    def password(self):
        """
        Gets a password to log on remote host.
        """
        return self._password

    @property
    def key(self):
        """
        Gets path for private key to be able log on remote host.
        """
        return self._key_path

    def __repr__(self):
        """
        Gets string representation for Credential instance.
        """
        return "<Credential: {0}>".format(self._username)


