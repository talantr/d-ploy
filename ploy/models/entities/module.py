from os.path import dirname
from .configurator import FlexConfig


#todo: add support of package managers
class Module(FlexConfig):
    """
    It's a base class for all imported software modules which are used to describe desired environment. Actually it
    provides an interface to get read-only attributes, like software name, version and etc.
    """

    __slots__ = ('_id',  '_resources', '_requires', '_description', '_version', '_pkg_manager')

    @staticmethod
    def create_subtype(name):
        """
        Create a subtype which can represent specified software type, for example HTTP or WSGI server.
        """
        return type(name, (Module,), dict(__slots__=tuple()))

    def __init__(self, mid, **settings):
        """
        Create an empty collection of resources with given meta-data.

        The `mid` is an identifier of creating collection of resources.

        The `settings` is a dictionary of meta parameters for the collection.
        """
        self._id = mid
        self._requires = settings.pop('requires', [])
        self._description = settings.pop('description', '')
        self._version = settings.pop('version', '')
        self._resources = []
        super(Module, self).__init__(**settings)

    @property
    def id(self):
        """
        Gets string identifier of this module.
        """
        return self._id

    @property
    def requirements(self):
        """
        Gets a collection of required modules for this one.
        """
        return self._requires

    @property
    def description(self):
        """
        Gets user defined description for this module
        """
        return self._description

    @property
    def version(self):
        """
        Gets a version of the software module
        """
        return self._version

    @property
    def logdir(self):
        """
        Gets a directory where log files are placed.
        """
        return dirname(getattr(self, 'log_files', '')) 

    @property
    def count(self):
        """
        Gets number of resources in the group.
        """
        return len(self._resources)

    def add(self, rsc):
        """
        Add new resource into this collection.
        """
        self._resources.append(rsc)

    def __iter__(self):
        """
        Enumerates all resources in this group.
        """
        for rsc in self._resources:
            yield rsc

    def __repr__(self):
        """
        Gets string representation of the module
        """
        return "<Module: %s>" % (self._id,)

    def __hash__(self):
        """
        Provides a hash for the module to support sets of modules.
        """
        return hash(self._id)

    def __eq__(self, other):
        return self.__class__ == other.__class__ and self._id == other._id


class _DPloyModuleType(Module):
    """
    Special module type for internal D-Ploy goals. For example to get details about remote host, like OS or platform
    name you can use an instance of the internal DescribeGoal class.
    """
    def __init__(self):
        """
        """
        super(_DPloyModuleType, self).__init__(':ploy', name=':ploy',
                                               version='1.0', description='Internal D-Ploy module')

DPloyModule = _DPloyModuleType()
