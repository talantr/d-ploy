import os
from getpass import getuser
from yaml import load as yaml_load
from ploy.models.artefacts.artefact import Credential
from ploy.models.entities.configurator import Environment, FlexConfig
from ploy.models.entities import Module, Product, Resource, Role, URI, Host

try:
    from yaml import CLoader as YamlLoader
except ImportError:
    from yaml import Loader as YamlLoader


def enum(**enums):
    return type('Enum', (), enums)

# Top-level keys in YAML configuration.
GlobalKeys = enum(PRODUCT='product',
                  ENVIRONMENT='environment',
                  ROLES='roles',
                  UNITS='modules')

ProductKeys = enum(ID='hid',
                   NAME='name',
                   DESCRIPTION='description')

# Sub-keys in environment section of YAML configuration.
EnvironmentKeys = enum(NAME='name',         # A short name of environment
                       CREDENTIAL='deployer',
                       BUILDHOST='build-host',
                       WORKSPACE='workspace',
                       BUILDSPACE='buildspace',
                       TEMPLATES='templates-home',
                       DEPLOYER='deployer')

RoleKeys = enum(HOSTS='hosts',
                MODULES='modules',
                SERVICES='services')

# Expected keys in definition of a module in YAML configuration.
ModuleKeys = enum(NAME='name',             # A name of a particle
                  DAEMON='daemon',
                  SUMMARY='description',
                  HOME='home',
                  VERSION='version',
                  DEPENDENCIES='requires',
                  LOGS='log-files',        # This is the log directory
                  CFGS='cfg-files')


class YamlProductParser(object):
    """
    This is a parser which builds an instance of Product class from YAML file.
    """

    @classmethod
    def set_default_keys(cls, p_spec):
        """
        Check that whether a specification is valid for given specification of a product.
        """
        if not GlobalKeys.PRODUCT in p_spec:
            raise Exception("YAML parser. Infrastructure section is not defined in the configuration.")
        YamlProductParser.set_default_product_keys(p_spec[GlobalKeys.PRODUCT])

        # If environment was not define then assign development
        p_spec[GlobalKeys.ENVIRONMENT] = p_spec.get(GlobalKeys.ENVIRONMENT, {})
        # YamlProductParser.set_default_environment(p_spec[GlobalKeys.ENVIRONMENT],
        #                                           p_spec[GlobalKeys.PRODUCT][ProductKeys.NAME])

        # Throw an exception if there is no definition for roles in YAML file
        if not GlobalKeys.ROLES in p_spec:
            raise Exception('YAML parser. The roles section is not defined in the configuration')

        # Throw an exception if there are no definition of particles in YAML file
        if not GlobalKeys.UNITS in p_spec:
            raise Exception('YAML parser. The definition of modules has not been defined in your configuration')

        YamlProductParser.set_default_roles(p_spec[GlobalKeys.ROLES], p_spec[GlobalKeys.UNITS])

    @classmethod
    def set_default_roles(cls, r_spec, u_spec):
        """
        Checks that all required attributes are defined for each role.
        """
        for role, meta in r_spec.items():
            meta[RoleKeys.HOSTS] = set(meta.get(RoleKeys.HOSTS, []))
            meta[RoleKeys.MODULES] = set(meta.get(RoleKeys.MODULES, []))
            meta[RoleKeys.SERVICES] = set(meta.get(RoleKeys.SERVICES, []))
            #assert type(meta) == list, "The role '%s' must define a list of units." % role
            for m in meta[RoleKeys.MODULES]:
                assert m in u_spec, \
                    "The module '%s' is used in definition of the role '%s', but it's not defined itself." % (m, role)
            # assert type(resources) == list, \
            #         "The particle '%s' doesn't contain resources in definition of the role '%s'." % (m, role)

    @classmethod
    def set_default_product_keys(cls, i_spec):
        """
        Checks that required attributes are defined for infrastructure and set default values another attributes.
        """
        if not ProductKeys.ID in i_spec:
            raise Exception("YAML parser. Product ID is not defined in the configuration.")
        i_spec[ProductKeys.NAME] = i_spec.get(ProductKeys.NAME, i_spec[ProductKeys.ID])
        i_spec[ProductKeys.DESCRIPTION] = i_spec.get(ProductKeys.DESCRIPTION, '')

    @staticmethod
    def extract_module_from_uri(uri):
        """
        Try to extract module name from URI scheme. An URI for each resource must have the scheme part in such format:
         module+protocol
        """
        i = uri.scheme.find('+')
        if i > 0:
            uri.absolute = uri.absolute[i+1:]
            return uri.scheme[:i], uri.scheme[i+1:]
        return uri.scheme, None

    def __init__(self, product, workspace=None):
        """
        Creates an
        """
        self._workspace = workspace
        self.product_cfg = product if isinstance(product, basestring) else None
        self.product_map = product if isinstance(product, dict) else {}

    def build_host(self, host):
        """
        Creates an instance of the Host type by hostname or IP address.
        """
        env = self.product_map[GlobalKeys.ENVIRONMENT]
        env.hosts[host] = env.hosts.get(host, Host(host))
        return env.hosts[host]

    def build_roles(self, roles, modules):
        """
        Create a set of services for each of described roles.
        An expected description of roles
        """
#        rsc_types = {}
        for role, meta in roles.items():
            hosts = map(self.build_host, meta.pop(RoleKeys.HOSTS))
            resources = dict((m, []) for m in set(meta.pop(RoleKeys.MODULES, [])))
            for s in meta.pop(RoleKeys.SERVICES, []):
                uri = URI(s)
                m, uri.scheme = self.extract_module_from_uri(uri)
                if m is None:
                    raise Exception("YAML parser. A module is undefined for resource definition, %s" % s)
                if m not in modules:
                    modules[m] = Module(m, name=m, requires=[])
                    # raise Exception("YAML parser. Unexpected module '%s' in the service declaration." % m)
                resources[m] = resources.get(m, [])
                resource_type = Resource.create_subtype(modules[m].__class__.__name__ + 'Rsc')
                for h in hosts:
                    resources[m].append(resource_type(None, modules[m], uri.localize(h), len(resources[m])))
            roles[role] = Role(role, resources, hosts, **meta)
        return roles

    def build_modules(self, m_spec, resolved={}, unresolved=set()):
        """
        Create a collection of modules using given specifications. It also resolves all dependencies
        for each imported module and include them in the retrived list.
        """
        for mid, settings in m_spec.items():
            if mid not in resolved and mid not in unresolved:
                unresolved.add(mid)
                settings[ModuleKeys.DAEMON] = settings.get(ModuleKeys.DAEMON, '[Empty]')
                settings[ModuleKeys.SUMMARY] = settings.get(ModuleKeys.SUMMARY, '')
                settings[ModuleKeys.HOME] = settings.get(ModuleKeys.HOME)
                settings[ModuleKeys.LOGS] = settings.get(ModuleKeys.LOGS)
                settings[ModuleKeys.CFGS] = settings.get(ModuleKeys.CFGS)
                name = settings[ModuleKeys.NAME] = settings.get(ModuleKeys.NAME, mid)
                dependencies = []
                # Resolving dependencies for current module
                for required in settings.get(ModuleKeys.DEPENDENCIES, []):
                    if required in unresolved:
                        raise Exception('Circular module dependencies detected')
                    if required not in resolved:
                        self.build_modules(m_spec, resolved, unresolved)
                    dependencies.append(resolved[required])
                settings[ModuleKeys.DEPENDENCIES] = dependencies
                module_type = Module.create_subtype(''.join(x.capitalize() for x in mid.split('-')))
                resolved[name] = resolved[mid] = module_type(mid, **settings)
                unresolved.remove(mid)
        return resolved

    @staticmethod
    def create_custom_object(spec, type=FlexConfig, list_item_handler=lambda x: x):
        for k, v in spec.items():
            if isinstance(v, dict):
                spec[k] = YamlProductParser.create_custom_object(v)
            elif isinstance(v, list):
                spec[k] = map(list_item_handler, v)
        return type(**spec)

    def build_environment(self, env):
        """
        Validate the given specification and create an Environment object if it's possible. It's required that
        the product ID has been assigined in the product configuration before validation.
        """
        if isinstance(env, Environment):
            return env
        pid = self.product_map[GlobalKeys.PRODUCT][ProductKeys.ID]

        env[EnvironmentKeys.NAME] = env.get(EnvironmentKeys.NAME, 'development')
        env[EnvironmentKeys.DEPLOYER] = env.get(EnvironmentKeys.DEPLOYER, {})
        credential = env[EnvironmentKeys.CREDENTIAL]
        if type(credential) != Credential:
            env[EnvironmentKeys.CREDENTIAL] = Credential(credential.get('login'),
                                                         credential.get('password'), credential.get('key'))
        env[EnvironmentKeys.WORKSPACE] = os.path.abspath(self._workspace or
                                                         env.get(EnvironmentKeys.WORKSPACE, os.getcwdu()))
        # Set /tmp/<product-id>-<user> as default build space, if it's not defined in the YAML.
        env[EnvironmentKeys.BUILDSPACE] = env.get(EnvironmentKeys.BUILDSPACE, '/tmp/%s-%s' % (pid, getuser()))
        # If template locations are not defined then it is used the templates subdirectory in the workspace.
        env[EnvironmentKeys.TEMPLATES] = env.get(EnvironmentKeys.TEMPLATES,
                                                 [env[EnvironmentKeys.WORKSPACE] + '/templates'])
        if isinstance(env[EnvironmentKeys.TEMPLATES], basestring):
            env[EnvironmentKeys.TEMPLATES] = [env[EnvironmentKeys.TEMPLATES]]
        else:
            assert isinstance(env[EnvironmentKeys.TEMPLATES], list), \
                "The '{0}' must have list or string types".format(EnvironmentKeys.TEMPLATES)
        self.product_map[GlobalKeys.ENVIRONMENT] = YamlProductParser.create_custom_object(env, Environment)
        return self.product_map[GlobalKeys.ENVIRONMENT]

    def build_product(self):
        """
        Opens specification in YAML file and creates the product object.
        """
        if not self.product_map:
            with file(self.product_cfg, 'r') as cfg_stream:
                self.product_map = yaml_load(cfg_stream, Loader=YamlLoader)
        YamlProductParser.set_default_keys(self.product_map)
        self.product_map[GlobalKeys.ENVIRONMENT] = self.build_environment(self.product_map[GlobalKeys.ENVIRONMENT])
        self.product_map[GlobalKeys.UNITS] = self.build_modules(self.product_map[GlobalKeys.UNITS])
        roles = self.build_roles(self.product_map.pop(GlobalKeys.ROLES), self.product_map[GlobalKeys.UNITS])
        self.product_map['_' + GlobalKeys.UNITS] = self.product_map.pop(GlobalKeys.UNITS)
        self.product_map['_' + GlobalKeys.ENVIRONMENT] = self.product_map.pop(GlobalKeys.ENVIRONMENT)
        return Product(roles, self.product_map)
