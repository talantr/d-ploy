from .configurator import FlexConfig
from .module import Module


class Role(FlexConfig):
    """
    The class represents a set of resources which are used together to provide some service.
    It is a logical unit for a system.

    Examples:
        - HDFS cluster may contain several severs and include few type software components (name nodes, data nodes),
        but it can be a single component on the logic level.
    """

    __slots__ = ('name', '_hosts', '_modules', '_service')

    def __init__(self, name, resources={}, hosts=[], **kwargs):
        """
        Creates a role with specified name.
          - name is a name of creating role;
          - units is a dictionary, for which  key is an unit (it is a Unit instance) and
            value is a list of endpoints for this unit.
        """
        self.name = name
        self._hosts = hosts
        self._modules = set()
        # TODO: Need to add required modules for given ones in the set
        for n, rsc in resources.items():
            self._modules.update(set(x.module for x in rsc))
            kwargs[n] = rsc
        super(Role, self).__init__(**kwargs)

    def modules(self):
        """
        A generator of software modules which associated with this role.
        """
        for m in self._modules:
            yield m

    def hosts(self, module):
        """
        A generator of hosts which associated with given module in this role.
        """
        for h in self._hosts:
            yield h

    def __iter__(self):
        """
        This is a generator to iterate by all particles/units for this role.
        """
        for name in self._settings:
            yield self._settings[name]

    def __getitem__(self, key):
        """
        Get an unit/particle with specified name.
        """
        key = key.name if isinstance(key, Module) else key
        if key in self._settings:
            return self._settings[key]
        raise KeyError("The role {0} doesn't contain module {1}".format(self.name, key))

    def __contains__(self, item):
        """
        Check that the role contains a specified unit/particle.
        """
        if isinstance(item, Module):
            return item.name in self._settings
        else:
            return item in self._settings

    def __repr__(self):
        """
        Returns string representation for a role
        """
        return "<Role: {0}>".format(self.name)
