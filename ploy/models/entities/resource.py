import copy
import os
from urlparse import urlparse


class Host(object):
    """
    The class represents a host in some environment. It is required to store additional information about host, like
    OS type, OS version and etc. It may be different jobs to archive a goal for different platforms (e.g the usage
    apt-get or yum for Linux distributions to install some package), so such info may help to choose concrete
    implementation of some task according target platform.
    """

    __slots__ = ('_hostname', '_platform', '_os', '_version', '_arch')

    def __init__(self, hostname):
        self._hostname = hostname
        self._platform = None
        self._os = None
        self._version = None
        self._arch = None

    @property
    def hostname(self):
        """
        Gets name or IP address for this host.
        """
        return self._hostname

    @property
    def platform(self):
        """
        Gets platform name (Linux, Windows, etc.).
        """
        return self._platform

    @property
    def os(self):
        """
        Gets OS name.
        """
        return self._os

    @property
    def version(self):
        """
        Gets OS version.
        """
        return self._version

    @property
    def architecture(self):
        """
        Gets a CPU architecture for the target host.
        """
        return self._arch

    @property
    def info(self):
        return self._os.capitalize() + " " + self._version + " (" + self._arch + ")"

    def __eq__(self, other):
        """
        Determines whether given object is a host with same name as this one.
        """
        return type(self) == type(other) and self.hostname == other.hostname

    def __hash__(self):
        """
        Gets a hash for the host.
        """
        return hash(self._hostname)

    def __repr__(self):
        """
        Gets a string representation of the host object.
        """
        return "<Host:%s>" % self._hostname

LOCALHOST = Host('127.0.0.1')


class URI(object):
    """
    The class represents Uniform Resource Identifier according RFC 3986.
    Now, it just wraps the urlparse.Result
    """

    DEFAULT_PORTS = {'ldap': 389, 'ftp': 21, 'ssh': 22, 'http': 80, 'https': 443, 'mysql': 3306}

    __slots__=('username', 'password', 'port', 'path', 'scheme', 'absolute', '_host')

    def __init__(self, uri):

        result = urlparse(uri)
        self.scheme = result.scheme
        self.username = result.username
        self.password = result.password or ''
        self._host = Host(result.hostname)
        self.port = result.port or self.DEFAULT_PORTS.get(self.scheme, 0)
        self.path = result.path
        self.absolute = uri

    def localize(self, host='127.0.0.1'):
        """
        Create a local version of a resource
        """
        u = copy.deepcopy(self)
        u._host = host if type(host) == Host else Host(host)
        u.absolute = u.absolute.replace(self.hostname, u.hostname, 1)
        return u

    @property
    def hostname(self):
        """
        Gets IP address or DNS name of the host where this resource is placed.
        """
        return self._host.hostname

    @property
    def host(self):
        """
        Gets Host object for this resource identifier.
        """
        return self._host

    def __str__(self):
        """
        Return a absolute URI as result of conversation to string
        """
        return self.absolute

    def __repr__(self):
        """
        Gets a string representation for the object.
        """
        return "<URI: %s>" % self.absolute

    def __hash__(self):
        return hash(self.absolute)

    def __eq__(self, other):
        return self.absolute == other.absolute


class Resource(object):
    """
    The class represents an information about particular service or another resource in a system.
    Each resource has an URI that can identify it.
    """

    __slots__ = ('_module', '_uri', 'index', 'role', 'islocal')

    @classmethod
    def create_subtype(cls, name):
        """
        Create a type with specified name to store configuration parameters.
        """
        return type(name, (cls,), dict(__slots__=tuple()))

    @staticmethod
    def create_internal_config_name(service, template_name):
        """
        """
        return "{0}-{1}.{2}".format(service.name, template_name, service.index)

    def __init__(self, role, module, uri, index, **settings):
        """
        Creates a new endpoint for specified URI.
        """
        self._module = module
        self._uri = uri if isinstance(uri, URI) else URI(uri)
        self.index = index
        self.role = role
        self.islocal = 'false'
        super(Resource, self).__init__(**settings)

    @property
    def uri(self):
        """
        Returns URI instance for this service.
        """
        return self._uri

    @property
    def hostname(self):
        """
        Returns URI instance for this service.
        """
        return self._uri.host.hostname

    @property
    def module(self):
        return self._module

    @property
    def host(self):
        """
        Gets a host where the endpoint is placed.
        """
        return self._uri.host

    @property
    def port(self):
        """
        Returns port number for service this endpoint
        """
        return self._uri.port

    @property
    def path(self):
        """
        Returns a path of the URI for service this endpoint
        """
        return self._uri.path

    @property
    def username(self):
        """
        Returns an username form URI for service this endpoint
        """
        return self._uri.username

    @property
    def password(self):
        """
        Returns a password form URI for service this endpoint
        """
        return self._uri.password

    @property
    def name(self):
        """
        Gets name of this service.
        """
        return self._module.name

    @property
    def daemon(self):
        """
        Gets daemon name for the service.
        """
        return self._module.daemon

    @property
    def home(self):
        """
        Gets a path where the unit has been installed.
        """
        return self._module.home

    @property
    def logs(self):
        """
        Gets a path to log files. The path may contain wildcard characters.
        """
        return self._module.logs

    @property
    def logdir(self):
        """
        Gets a directory where log files are stored.
        """
        return self._module.logdir

    @property
    def configs(self):
        """
        Gets a collection of configuration files for a service
        """
        return self._module.cfg_files

    def cfgnames(self):
        """
        Gets a names of configuration files.
        """
        return map(lambda x: os.path.split(x)[1], self._module.cfg_files)

    # @property
    # def count(self):
    #     return len(self._module.endpoints)

    def __repr__(self):
        return "<{0} #{1}: {2}:{3}{4}>".format(self.name, self.index, self.host.hostname, self.port, self._uri.path)

    def __hash__(self):
        return hash(str(self.index) + self.name + self.host.hostname + str(self.port))

    def __eq__(self, other):
        return self.name == other.name and self._uri == other.uri and self.index == other.index
