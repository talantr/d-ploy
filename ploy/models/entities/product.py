# -*- coding: utf-8 -*-
"""
Created on 14.07.2012
Author: Anton Talevnin
"""
import os
from .role import Role


class Product(object):
    """
    The class represent specification for a product that will be deployed or monitored.
    Also it provides an interface to represent the product as collection of roles.
    """

    def __init__(self, roles, spec):
        """
        Create a product using a specification.
        """
        self._roles = roles
        for name, role in roles.items():
            self.__dict__[name] = role
        self.__dict__.update(spec)

    @property
    def name(self):
        """
        Gets name of the product.
        """
        return self.product

    @property
    def environment(self):
        """
        Gets name of the product environment.
        """
        return self._environment

    @property
    def modules(self):
        """
        Gets dictionary of <unit name> : <unit object> pairs for the product.
        """
        return self._modules

    def get_module(self, id):
        """
        Gets an unit with specified name.
        """
        return self._modules.get(id, None)

    def __iter__(self):
        """
        Gets an iterator by all roles
        """
        return self._roles.itervalues()

    def __getitem__(self, key):
        """
        Get an unit with specified key.
        """
        key = key.name if isinstance(key, Role) else key
        if key in self._roles:
            return self.__dict__[key]
        raise KeyError("Product '{0}' doesn't contain role {1}".format(self.name, key))

    def __contains__(self, role):
        """
        Check that the product contains a specified role.
        """
        if isinstance(role, Role):
            return role.name in self._roles
        else:
            return role in self._roles


from jinja2 import Environment as JinjaEnvironment, FileSystemLoader, TemplateNotFound
class JinjaEngine(object):
    """
    The template engine to process configuration files. This is a wrapper for Jinja2 framework
    """
    __slots__=('_engine', '_context' , '_product')


    def __init__(self, product):
        """
        Initialize engine and set up paths to search for templates
        """
        self._engine = JinjaEnvironment(loader=FileSystemLoader(map(lambda x: x, product.environment.template_locations())))
        self._context = {}
        self._product = product

    def prepare(self, service):
        self.update_ctx(**{'this' : service, 'product' : self._product, 'environment' : self._product.environment})
        out_dir = os.path.join(self._product.environment.workspace, service.hostname)

        if not os.path.exists(out_dir):
            os.mkdir(out_dir)

        for cfg_name in service.configs:
            cfg_file_name = os.path.split(cfg_name)[1]
            try:
                template = self._engine.get_template(cfg_file_name)
#                cfg_file_name = os.path.join(out_dir, Endpoint.create_internal_config_name(service, cfg_file_name))
                cfg_file_name = os.path.join(out_dir, cfg_file_name)
                with open(cfg_file_name, 'w') as config:
                    print >> config, template.render(**self._context)
            except TemplateNotFound:
                pass # skip configuration file if their template is not found

    def update_ctx(self, **kwargs):
        self._context.update(kwargs)

    def prepare_one(self, service, cfg_name, target=None):
        self.update_ctx(**{'this' : service, 'product' : self._product, 'environment' : self._product.environment})
        out_dir = os.path.join(self._product.environment.workspace, service.hostname)

        if not os.path.exists(out_dir):
            os.mkdir(out_dir)

        cfg_file_name = os.path.split(cfg_name)[1]
        try:
            template = self._engine.get_template(cfg_file_name)
            cfg_file_name = os.path.join(out_dir, target if target else cfg_file_name)
            with open(cfg_file_name, 'w') as config:
                print >> config, template.render(**self._context)
        except TemplateNotFound:
            pass # skip configuration file if their template is not found

