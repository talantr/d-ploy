
class FlexConfig(object):
    """
    This class implements expansible storage for deployment settings of different entities.
    All properties are accessible in read-only mode via properties which are created for each declared parameter in the Yaml.
    Don't use this class to create object, create descendants or use the CustomConfig.create_subtype to declare new types.
    """

    __slots__ = '_settings'

    @staticmethod
    def create_subtype(name):
        """
        Create a type with specified name to store configuration parameters.
        """
        return type(name, (FlexConfig,), dict(__slots__=tuple()))

    @staticmethod
    def create_property(key):
        """
        Create a property to extract specified value from internal dictionary
        """
        return property(lambda x: x._settings[key])

    def __init__(self, **kwargs):
        """
        Creates a configuration storage with specified parameters.
        kwargs - a dictionary of settings
        """
        self._settings = kwargs
        # Add read-only properties dynamically
        for k in kwargs:
            setattr(self.__class__, k.replace('-', '_'), self.create_property(k))

    def __getitem__(self, item):
        """
        Get an attribute from internal storage like dictionary mode.
        """
        return self._settings[item]

    def __setitem__(self, key, value):
        """
        Change value of particular property.
        """
        self._settings[key] = value

    def set(self, key, value):
        self._settings[key] = value
        setattr(self.__class__, key.replace('-', '_'), self.create_property(key))


class Context(FlexConfig):

    def reset(self, settings={}):
        self._settings = settings
        # Add read-only properties dynamically
        for k in settings:
            setattr(self.__class__, k.replace('-', '_'), self.create_property(k))


class Environment(FlexConfig):
    """
    This class provides global settings for deployment process.
    It always contains several required attributes:
      - name is an environment name
      - deployer is an instance of the Credential class to provide credentials on remote hosts
      during deployment process.
      - templates contains a list of paths where templates for configuration files are looked for.
      - workspace - a working directory on the local host.
    """

    __slots__ = 'hosts'

    def __init__(self, **settings):
        self.hosts = {}
        super(Environment, self).__init__(**settings)

    def template_locations(self):
        """
        This generator returns full path for each directory in the templates_home property.
        Just for Linux hosts.
        """
        for path in self.templates_home:
            yield path if path[0] == '/' else self.workspace + '/' + path
