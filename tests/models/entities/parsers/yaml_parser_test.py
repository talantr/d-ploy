"""
This module contains a suite of tests with INSERT statements for accounts.
"""
import os
import unittest

from ploy.models.entities.parsers.yaml_parser import YamlProductParser
from ploy.models.entities import Host
from ploy.models.entities.configurator import Environment
import tests


class YamlParserBaseTest(unittest.TestCase):
    """
    A suite of tests to check creation a product and its components from YAML format.
    """

    parser = YamlProductParser(os.path.join(os.path.dirname(tests.__file__), 'example.yaml'))

    @classmethod
    def setUpClass(cls):
        cls.modules = {"application": {"version": "0.4", "home": "/opt/application"},
                       "nginx": {},
                       "uwsgi": {}}

        cls.env = {"name": "live",
                   "workspace": "/opt/workspace/ploy-example",
                   "buildspace": "/tmp/ploy-example",
                   "build-host": "127.0.0.8",
                   "deployer": {"login:": "toor", "password": 'dr0wssap' }}

        cls.roles = {"app": {"modules": ["application", "nginx", "uwsgi", "uwsgi"],
                             "hosts": ["stg.example.net", "example.net"],
                             "services": ["application://${host}/", "uwsgi+tcp://${host}:9000/"]},
                     "app-tst": {"hosts": ["stg.example.com"],
                                 "services": ["nginx+tcp://127.0.0.1:80/"]}}

        cls.yml_parser = YamlProductParser({'product': {'hid': 'tst', 'name': 'test'},
                                            'environment': cls.env,
                                            'roles': cls.roles,
                                            'modules': cls.modules})

    def build_environment_test(self):
        """
        YamlParser. Check that the environment section in the YAML declaration is parsed correctly and all defined
        attributes are converted in corresponding class instances.
        """
        env = self.yml_parser.build_environment(self.env)
        self.assertIsInstance(env, Environment, "Unexpected type of an environment object")
        self.assertEqual(env.name, self.env['name'], "Unexpected name in parsed environment")
        self.assertEqual(env.workspace, self.env['workspace'],
                         "Unexpected value for 'workspace' attribute in parsed environment")
        self.assertEqual(env.buildspace, self.env['buildspace'],
                         "Unexpected value for 'buildspace' attribute in parsed environment")
        self.assertEqual(env.build_host, self.env['build-host'],
                         "Unexpected value for 'build_host' attribute in parsed environment")

    def build_modules_test(self):
        """
        YamlParser. Check building modules.

        """
        m = self.yml_parser.build_modules(self.modules)
        self.assertEqual(len(self.modules), len(m), "Unexpected number of modules was parsed")
        self.assertEqual(m['application'].version, '0.4', "Unexpected version number for a module")
        self.assertEqual(m['application'].home, "/opt/application", "Unexpected version number for a module")

    def build_roles_test(self):
        """
        YamlParser. Check that all roles are parsed correctly.

        The test dependce from
        """
        parser = YamlProductParser({'product': {'hid': 'tst', 'name': 'test'},
                                    'environment': self.env})
        roles = {"app": {"modules": ["application", "nginx", "uwsgi", "uwsgi"],
                         "hosts": ["stg.example.net", "example.net"],
                         "services": ["application://${host}/", "uwsgi+tcp://${host}:9000/"]},
                 "app-tst": {"hosts": ["stg.example.com"],
                             "services": ["nginx+tcp://127.0.0.1:80/"]}}
        modules = {"application": {"version": 2.3}, "nginx": {}, "uwsgi": {}}
        parser.build_environment(self.env)
        modules = parser.build_modules(modules)
        # print "Environment: ", parser.product_map['environment']
        r = parser.build_roles(roles, modules)
        self.assertIn('app', r, "Expected role 'app' name is not in dictionary with roles.")
        self.assertIn('app-tst', r, "Expected role 'app-tst' name is not in dictionary with roles.")
        self.assertItemsEqual([], r['app'].nginx)
        self.assertSequenceEqual(['nginx'], map(lambda x: x.name,r['app-tst'].modules()),
                                 "Unexpected collection of modules for role 'app-test'.")

    def build_product_test(self):
        """
        YamlParser. Check building entire product.

        """
        p = self.yml_parser.build_product()
        self.assertSetEqual(set([Host('stg.example.net'), Host('example.net')]), set(p.app.hosts("application")),
                            "Collection of hosts is parsed incorrectly")
        modules = ['application', 'uwsgi']
        self.assertItemsEqual(modules, [m.id for m in p.app.modules()], "Collection of modules is parsed incorrectly")
        #Check types
        # self.assertItemsEqual([Module], [m.id for m in p.app.modules()], "Collection of modules is parsed incorrectly")
