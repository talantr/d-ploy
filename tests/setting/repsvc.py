# Implementation of a Python client for Report service

import httplib
import requests
import json
import time
from mocks.common.entities import dumping


class ReportService(object):
    """
    This class represents a Python implementation of a client for Marin Pull Report Service.
    It wraps all REST methods of the service for convenient usage in integration tests.

    https://docs.google.com/a/thumbtack.net/document/d/1fjW6a_psQUdLP7Hw7r6osPK90fkXDaApmA6S9B730bU
    """

    HEADERS = {'content-type': 'application/json'}
    TIMEOUT = 10

    def __init__(self, url):
        """
        Initialize a client for Pull Request Service with specified URL.
        """
        self._url = url

    def request(self, report):
        response = requests.post(self._url + '/report',
                                 data=json.dumps(dumping(report)),
                                 headers=self.HEADERS, timeout = self.TIMEOUT)
        if response.status_code == requests.codes.ok:
            report.id = response.json().get('id')
        else:
            response.raise_for_status()

    def error_code (self, report):
        response = requests.post(self._url + '/report',
                                 data=json.dumps(dumping(report)),
                                 headers = self.HEADERS)
        report.info=dict()
        report.info['code'] = response.status_code
#        try:
#            report.info['message'] = json.loads(response.text).get('internalCode')
#        except:
#            report.info['message'] = ''

    def status(self, report):
        response = requests.get(self._url + '/report/' + str(report.id) + '/status')
        if response.status_code == requests.codes.ok:
            response = response.json()
            report.status=response.get('status')
            report.info=response.get('errorInfo')

    def file(self, report):
        if report.status == 'COMPLETED':
            response=requests.get(self._url + '/report/' + str(report.id) + '.csv')
            if response.status_code==httplib.OK:
                report.file=response.text

    def wait_completed_status(self, r):
        self.status(r)
        while r.status not in (r.statuses.COMPLETED, r.statuses.FAILED):
            time.sleep(1)
            self.status(r)


