from mocks.common import Entity


def enum(**enums):
    return type('Enum', (), enums)

# Enumeration of possible exit codes for running jobs
ReportType = enum(KEYWORD=0, ADGROUP=1, CAMPAIGN=2)


class Credentials(Entity):
    """
    It represents credentials for external services (Yandex, Twitter) in integration tests.
    """

    def __init__(self, **kwargs):
        """
        Initialize specified type of credentials.
        """
        super(Credentials, self).__init__(**kwargs)


class ServiceStatus(object):
    """
    A set of allowed statuses for reports of Pull Report Service.
    """
    FAILED = 'FAILED'
    COMPLETED = 'COMPLETED'


class Report(Entity):

    statuses = ServiceStatus()
    __slots__ = ('id', 'status', 'info', 'file')

    def __init__(self, publisher, credentials, **kwargs):
        """
        Initializes a set of given attributes of the report by specified values.
        """
        self.id = None
        self.status = None
        self.info = dict()
        super(Report, self).__init__(publisherId = publisher, credentials = credentials, **kwargs)


class ReportForErrors(Entity):

    statuses = ServiceStatus()
    __slots__ = ('id', 'status', 'info')

    def __init__(self, **kwargs):
        """
        Initializes a set of given attributes of the report by specified values for checking errors code in case of invalid input data.
        """
        self.id = None
        self.status = None
        self.info = dict()
        super(ReportForErrors, self).__init__(**kwargs)
