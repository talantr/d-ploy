# The settings for Pull Report Service for tests
REPORT_SVC = None

# The settings for Yandex Market Emulator
YANDEX_API = None

# The settings for Twitter API emulator
TWITTER_API = None

# Test settings
TEST = None


class TestSettings(object):
    """
    This class implements expansible storage for test settings.
    """

    __slots__ = '_settings'

    @staticmethod
    def create_subtype(name):
        """
        Create a type with specified name to store configuration parameters.
        """
        name = name.replace(' ', '')
        return type(name, (TestSettings,), dict(__slots__=tuple()))

    @staticmethod
    def create_property(key):
        """
        Create a property to extract specified value from internal dictionary
        """
        return property(lambda x: x._settings[key])

    def __init__(self, **kwargs):
        """
        Creates a configuration storage with specified parameters.
        kwargs - a dictionary of settings
        """
        self._settings = kwargs
        # Add read-only properties dynamically
        for k in kwargs:
            setattr(self.__class__, k.replace('-', '_'), self.create_property(k))

