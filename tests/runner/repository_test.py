"""
Implementation of the test suite to check import jobs functionality
"""
import os
import unittest

from ploy.models.entities.parsers import YamlProductParser
from ploy.runner.configurator import YamlContextConfigurator
from ploy.api import Context, JobFilter
from ploy.runner import registry, JobRegistry
from ploy.runner.repository import LocalFileRepo
import tests


class ImportGoalsTest(unittest.TestCase):
    """
    A suite of tests to check creation a product and its components from YAML format.
    """
    TEST_LOCATION = os.path.dirname(tests.__file__)
    TEST_REPO_PATH = os.path.join(TEST_LOCATION, 'modules')
    TEST_ENV_YML = os.path.join(TEST_LOCATION, 'example.yaml')
    PLOY_CONFIG = os.path.join(TEST_LOCATION, '..', 'etc', 'd-ploy.cfg')

    @classmethod
    def setUpClass(cls):
        parser = YamlProductParser(cls.TEST_ENV_YML)
        cls.ctx = Context(product=parser.build_product())
        registry.jobs = JobRegistry(cls.ctx.product)

    def job_filter_test(self):
        """
        Configurator. Check building environment, all attributes are provided.
        """
        sieve = JobFilter(self.ctx.product, roles=['app-00'], modules=[], hosts=[])
        print sieve.roles, sieve.modules, sieve.hosts

    def import0_test(self):
        """
        Configurator. Check building environment, all attributes are provided.
        """
        sieve = JobFilter(self.ctx.product, roles=['app-00'], modules=[], hosts=[])
        configurator = YamlContextConfigurator(self.ctx, self.PLOY_CONFIG)
        for repo in configurator.repositories():
            repo.import_tasks(sieve)

        for job in registry.jobs:
            print job

    def import_all_tasks_test(self):
        """
        Import all tasks from local files
        """
        registry.jobs = JobRegistry(self.ctx.product)
        repository = LocalFileRepo(self.ctx.product, self.TEST_REPO_PATH)
        repository.import_tasks()
        print "Hey", registry.jobs._goals
        for job in registry.jobs:
            print job

    def import_shared_tasks_test(self):
        """
        Import all tasks from local files
        """
        registry.jobs = JobRegistry(self.ctx.product)
        repository = LocalFileRepo(self.ctx.product, self.TEST_REPO_PATH)
        repository.import_tasks()
        self.assertEqual(4, len(filter(lambda j: j.name == 'summary', registry.jobs)))
        # for job in filter(lambda j: j.name == 'summary', registry.jobs):
        #     print job



