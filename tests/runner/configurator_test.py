"""
This module contains a suite of tests with INSERT statements for accounts.
"""
import os
import unittest

from ploy.models.entities.parsers import YamlProductParser
from ploy.runner.configurator import YamlContextConfigurator
import tests


class ConfiguratorTest(unittest.TestCase):
    """
    A suite of tests to check creation a product and its components from YAML format.
    """

    TEST_ENV_YML = os.path.join(os.path.dirname(tests.__file__), 'example.yaml')
    PLOY_CONFIG = os.path.join(os.path.dirname(tests.__file__), '../etc/d-ploy.cfg')

    @classmethod
    def setUpClass(cls):
        pass

    def configure_test(self):
        """
        Configurator. Check building environment, all attributes are provided.
        """
        parser = YamlProductParser(self.TEST_ENV_YML)
        product = parser.build_product()
        ploy_cfg = YamlContextConfigurator(product, self.PLOY_CONFIG)
        for x in ploy_cfg.jobs():
            print x.name, x.target, x.on_success, x.on_fail
        # print ploy_cfg.repositories()
