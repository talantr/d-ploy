"""
This module contains a suite of tests with INSERT statements for accounts.
"""

from ploy.models.entities.parsers.yaml_parser import YamlProductParser
from ploy.models.entities import LOCALHOST
from ploy.runner import JobFilter
import itertools
import unittest


class FilterTest(unittest.TestCase):
    """
    """

    @classmethod
    def setUpClass(cls):
        cls.modules = {"app-binary": {},
                       "app-00-svc": {"version": "0.0", "requires": ['app-binary']},
                       "app-01-svc": {"version": "0.1", "home": "/opt/app01", "requires": ['app-binary']},
                       "app-02-svc": {"version": "0.2", "home": "/opt/app02", "requires": ['app-binary']},}

        cls.env = {"name": "live"}

        cls.roles = {"app-00": {"hosts": ["127.0.0.100", "127.0.0.101", "127.0.0.102"],
                                "services": [
                                    "app-00-svc+http://${host}/"
                                ]},
                     "app-01": {"hosts": ["127.0.0.100", "127.0.0.201", "127.0.0.202"],
                                "services": [
                                    "app-01-svc+tcp://127.0.0.1:80/"
                                ]},
                     "app-02": {"hosts": ["127.0.0.200", "127.0.0.201", "127.0.0.202"],
                                "services": [
                                    "app-02-svc+tcp://127.0.0.1:80/"
                                ]}
                    }

        cls.yml_parser = YamlProductParser({'product': {'hid': 'tst', 'name': 'test'},
                                            'environment': cls.env,
                                            'roles': cls.roles,
                                            'modules': cls.modules})
        cls.product = cls.yml_parser.build_product()

    def module_closure(self, closure, *modules):
        closure.update(modules)
        for m in modules:
            requirements = filter(lambda r: r not in closure, m.requirements)
            closure.update(self.module_closure(closure, *requirements))
        return closure

    def filter_by_role_test(self):
        """
        """
        # A list of role names as test input
        role_names = ['app-00']
        # Gets Role instances for given role names
        roles = [self.product[name] for name in role_names]
        # Create a filter
        sieve = JobFilter(self.product, roles=role_names, modules=[], hosts=[])

        # Check that all roles in filter are same as expected ones
        self.assertItemsEqual(roles, sieve.roles)

        # Check that all modules and their requirements are in filter
        module_generators = [r.modules() for r in roles]
        modules = set(m for m in itertools.chain(*module_generators))
        modules = self.module_closure(set(), *modules)
        self.assertItemsEqual(modules, sieve.modules)

        # Check that all required hosts are in filter
        host_generators = []
        for r in roles:
            host_generators += [r.hosts(m) for m in r.modules()]
        hosts = set(h for h in itertools.chain(*host_generators))
        self.assertItemsEqual(set([LOCALHOST]), hosts ^ sieve.hosts)

