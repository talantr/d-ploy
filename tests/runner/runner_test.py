"""
This is implementation of a test suite to check the implementation for JobRunnerPool class

All tests in this suite sould be passed.
"""
from time import sleep, time
import unittest

from ploy.api import Task, Module, LOCALHOST
from ploy.runner.runner import JobRunnerPool, PoolUsageError


def fail_job(host):
    """
    A job example with raising an exception
    """
    raise Exception("Raise test exception")


def good_job(host):
    """
    A job example which finishes successfully
    """
    return 0


def wait_job(host, wait_time):
    """
    A job example which finishes successfully
    """
    sleep(wait_time)


def task(method, *arg, **kwargs):
    """
    A helper to produce tasks acceptable by pool of executors
    """
    return Task(method, LOCALHOST, Module('test'), *arg, **kwargs)


class PoolExecutorTest(unittest.TestCase):
    """
    A suite of tests to check job processing by pool of executors.
    """

    @classmethod
    def setUpClass(cls):
        cls.module = Module('test')

    def add_job_before_starting_pool_test(self):
        """
        Try to add a task if a pool is not running.

        The pool uses a queue to store tasks, the queue may have max size, so if you will add tasks before pool starting
          you may block task execution if the max size of queue will be reached.
        """
        pool = JobRunnerPool(size=2)
        self.assertRaises(PoolUsageError, pool.add, task(good_job, LOCALHOST))
        pool.wait()

    def start_good_job_test(self):
        """
        Run successfully executing task
        """
        pool = JobRunnerPool(size=2)
        pool.start()
        pool.add(task(good_job, LOCALHOST))
        pool.wait()

    def start_fail_job_test(self):
        """
        Run a task which throws exception.
        """
        pool = JobRunnerPool(size=2)
        pool.start()
        pool.add(task(fail_job, LOCALHOST))
        result = pool.wait()

    def start_wait_job_test(self):
        """
        """
        wait_time = 2
        pool = JobRunnerPool(size=2)
        pool.start()
        pool.add(task(wait_job, LOCALHOST, wait_time))
        start = time()
        pool.wait()
        self.assertGreaterEqual(time() - start, wait_time)

    def process_tasks_test(self):
        """
        Simple test to check that failed jobs are captured in the pool results
        """
        pool = JobRunnerPool(size=2)
        fail = task(fail_job, LOCALHOST)
        good = task(good_job, LOCALHOST)
        pool.start()
        pool.add(good)
        pool.add(fail)
        pool.add(fail)
        pool.add(good)
        result = pool.wait()
        self.assertEqual(2, len(result), "Two jobs must be finished raising exceptions")
        self.assertItemsEqual([fail, fail], map(lambda x: x[0], result),
                              "Fail jobs must be encountered in the result list only")

    def start_running_pool_test(self):
        """
        Try to start pool while it's already running.

        Expected result: the PoolUsageError exception must be raised.
        """
        def start_running_pool(pool):
            pool.start()
            pool.add(task(wait_job, LOCALHOST))
            pool.start()

        pool = JobRunnerPool(size=2)
        self.assertRaises(PoolUsageError, start_running_pool, pool)
        pool.wait()

    def interrupt_running_pool_test(self):
        """
        Try to interrupt task execution in pool immediately.

        Expected result: the PoolUsageError exception must be raised.
        """
        wait_time = 2
        pool = JobRunnerPool(size=2)
        pool.start()
        pool.add(task(wait_job, LOCALHOST, wait_time))
        start = time()
        pool.interrupt()
        delay = time() - start
        self.assertLess(delay, wait_time)
