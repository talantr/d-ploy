# -*- coding: utf-8 -*-
from ploy.api import job, JobTargetType
from fabric.api import puts


@job(JobTargetType.LOCAL)
def build(host):
    """
    Preparing DEB or RPM packages with bidder components.
    """
    puts("Building App 00 binaries ({0})...".format(ENV.workspace))


@job(JobTargetType.HOST)
def summary(host):
    """
    Gets summary for installed environment
    """
    puts("Hey, I'm summary...")
