@echo off

SET WORKSPACE=%~dp0
SET VIRTENV=D:\Temp\ploy

rem Build source tarball for the D-Ploy project
python setup.py sdist --formats=gztar
for /f "delims=" %%a in ('python -c "import ploy; print ploy.__version__"') do @set PLOY_VER=%%a

rem Uninstall old version of the package and install new one
%VIRTENV%\Scripts\pip uninstall -y d-ploy
%VIRTENV%\Scripts\pip install %WORKSPACE%dist\d-ploy-%PLOY_VER%.tar.gz