# D-Ploy. User guide

**D-ploy** is a set of python scripts to run deployment or systems administration tasks using
the [Fabric library](http://docs.fabfile.org/en/1.5/). However, D-Ploy uses meta data about
an environment which must be described in [YAML format](http://www.yaml.org/).

## Requirements

So D-Ploy is a python tool, the Python 2.6 (or 2.7) must be installed on your machine. Also you need following dependencies:

 - Docopt
 - Fabric 1.5+, [installation guide](http://docs.fabfile.org/en/1.5/installation.html)
 - PyYAML 3.0+, [installation guide](http://pyyaml.org/wiki/PyYAMLDocumentation#Installation)
 - Jinja2 2.6+, [installation guide](http://jinja.pocoo.org/docs/intro/#installation)

## Todo

    * pool of executors

## Changelog

2013-12-02  Anton Talevnin  <talantr@gmail.com>

    * Resolving dependencies for modules;

2013-12-02  Anton Talevnin  <talantr@gmail.com>

    * Fixed an issue with installation python virtualenv;
    * Improved job import, jobs from a filter are imported only;
    * Installation scripts for the python virtualenv;

2013-11-29  Anton Talevnin  <talantr@gmail.com>

    * Added default ports for http(s), ftp, mysql, ssh, ftp, ldap protocols

    * Fix an issue with empty module name in URI in service definition;

    * The modules keyword is optional in a role definition;

    * Added jinja2 as dependency in setup.py


2013-10-26  Anton Talevnin  <talantr@gmail.com>

    * implemented basic functionality of the d-ploy;
